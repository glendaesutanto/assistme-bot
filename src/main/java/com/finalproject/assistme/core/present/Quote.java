package com.finalproject.assistme.core.present;

public class Quote {

  private String message;

  public Quote(String message) {
    this.message = message;
  }

  public String getQuote() throws InterruptedException {
    return this.message;
  }
  
}
