package com.finalproject.assistme.core.flexmessage;

import static java.util.Arrays.asList;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.BubbleStyles;
import com.linecorp.bot.model.message.flex.container.BubbleStyles.BlockStyle;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.text.SimpleDateFormat;

public class ReminderCard {

  /**
   * Reminder card flex message.
   * @param event event to be reminded
   * @param reminderMessage reminder message
   * @return FlexMessage
   */
  public FlexMessage get(Event event, String reminderMessage) {

    LineUser user = event.getUser();
    String userDisplayName = user.getDisplayName();

    final Box bodyBlock = createBodyBlock(event, reminderMessage, userDisplayName);

    final Box footerBlock = createFooterBlock();

    final BubbleStyles bubbleStyle = createBubbleStyle();

    final Bubble bubble =
        Bubble.builder()
            .body(bodyBlock)
            .footer(footerBlock)
            .styles(bubbleStyle)
            .build();

    return new FlexMessage("AssistMe-Bot sent you a reminder!", bubble);
  }

  private Box createBodyBlock(Event event, String reminderMessage, String user) {

    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    String eventIdString = String.valueOf(event.getEventId());
    String dueDateAsString = dateFormatter.format(event.getDueDateTime());
    String reminderTimeAsString = dateFormatter.format(event.getReminderTime());

    final Text headerText = Text.builder()
        .text(String.format("HELLO, %s", user.toUpperCase()))
        .size(FlexFontSize.SM)
        .color("#c18cdb")
        .weight(TextWeight.BOLD)
        .wrap(true)
        .build();

    final Separator separator = Separator.builder().margin(FlexMarginSize.MD).build();

    final Text title1Text = Text.builder()
        .text("You have a new reminder!")
        .margin(FlexMarginSize.MD)
        .align(FlexAlign.CENTER)
        .weight(TextWeight.BOLD)
        .size(FlexFontSize.Md)
        .build();

    final Text title2Text = Text.builder()
        .text(String.format("Reminder for event #%s", eventIdString))
        .margin(FlexMarginSize.MD)
        .size(FlexFontSize.SM)
        .align(FlexAlign.CENTER)
        .weight(TextWeight.BOLD)
        .build();  

    final Text eventName = Text.builder()
        .text(event.getName())
        .margin(FlexMarginSize.MD)
        .size(FlexFontSize.Md)
        .weight(TextWeight.BOLD)
        .action(new MessageAction(event.getName(), "/getEvent " + eventIdString))
        .build();  

    final Box dueDateBox = createHorizontalInfoBox("Due date", dueDateAsString, "#111111");
    final Box reminderTimeBox = createHorizontalInfoBox("Reminder time", 
        reminderTimeAsString, "#111111");
    final Box ownerBox = createHorizontalInfoBox("Set by", user, "#aaaaaa");

    final Box infoBox = Box.builder()
        .layout(FlexLayout.VERTICAL)
        .margin(FlexMarginSize.MD)
        .spacing(FlexMarginSize.SM)
        .contents(asList(dueDateBox, reminderTimeBox, ownerBox))
        .build();

    final Text messageLabelText = Text.builder()
        .text("Message")
        .size(FlexFontSize.SM)
        .weight(TextWeight.BOLD)
        .margin(FlexMarginSize.MD)
        .build();

    final Text reminderMessageText = Text.builder()
        .text(reminderMessage)
        .margin(FlexMarginSize.MD)
        .size(FlexFontSize.SM)
        .wrap(true)
        .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(headerText, separator, title1Text, title2Text, separator,
            eventName, separator,infoBox, separator, messageLabelText, reminderMessageText))
        .build();
  }

  private Box createHorizontalInfoBox(String label, String content, String color) {

    final Text labelText = Text.builder()
        .text(label)
        .size(FlexFontSize.SM)
        .color(color)
        .flex(0)
        .wrap(true)
        .align(FlexAlign.START)
        .build();

    final Text contentText = Text.builder()
        .text(content)
        .size(FlexFontSize.SM)
        .color(color)
        .wrap(true)
        .align(FlexAlign.END)
        .build();

    return Box.builder()
        .layout(FlexLayout.HORIZONTAL)
        .contents(asList(labelText, contentText))
        .build();
  }

  private Box createFooterBlock() {

    final Text footerText = Text.builder()
        .text("P.S. Click the name of the event to get the details about the event!")
        .size(FlexFontSize.XS)
        .wrap(true)
        .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(footerText))
        .build();
  }

  private BubbleStyles createBubbleStyle() {

    return BubbleStyles.builder()
      .footer(BlockStyle.builder()
              .separator(true).build())
      .build();
  }
}