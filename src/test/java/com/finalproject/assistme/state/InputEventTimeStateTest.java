package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InputEventTimeStateTest {

  @Mock
  LineUserRepository lineUserRepository;

  @Mock
  EventRepository eventRepository;

  @Spy
  @InjectMocks
  private InputEventTimeState stateTest;

  @Test
  public void contextLoads() throws Exception {
    assertThat(this.stateTest).isNotNull();
  }

  @Before
  public void setUp() {
    LineUser userTest = new LineUser("1", "test", "test");
    userTest.setState(InputEventTimeState.DB_COL_NAME);
    Calendar cal = Calendar.getInstance();
    //2020-05-26 12:30:30
    cal.set(2020, 05, 26, 12, 30, 30);
    Date date = cal.getTime();
    Event eventTest = new Event(userTest, "test", "test", date);

    lineUserRepository.save(userTest);
    eventRepository.save(eventTest);

    when(eventRepository.findUnverifiedEventByUserId("1")).thenReturn(eventTest);
    when(eventRepository.findUnverifiedEventByUserId("2")).thenReturn(null);
    when(lineUserRepository.findLineUserByUserId("1")).thenReturn(userTest);
  }

  @Test
  public void createEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.createEvent("1"), response);
  }

  @Test
  public void cancelEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.CANCEL_SUCCESS);
    assertEquals(stateTest.cancelEvent("1"), response);
  }

  @Test
  public void deleteEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.deleteEvent("1", "1"), response);
  }

  @Test
  public void othersTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);

    assertEquals(
      stateTest.others(new String[] {"2100-05-27", "17:30:30"}, "1"),
      response
    );
  }

  @Test
  public void othersTestBefore() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);

    assertEquals(
      stateTest.others(new String[] {"2020-05-10", "17:30:30"}, "1"),
      response
    );
  }

  @Test
  public void othersTestNull() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);

    assertEquals(
      stateTest.others(new String[] {"2020-05-27", "17:30:30"}, "2"),
      response
    );
  }

  @Test
  public void othersTestParse() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);

    assertEquals(
      stateTest.others(new String[] {"error"}, "1"),
      response
    );
  }

  @Test
  public void reportMoodTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.reportMood(), response);
  }

  @Test
  public void processMoodTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.processMood("happy"), response);
  }

  @Test
  public void helpTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.HELP_TEXT);
    assertEquals(stateTest.help(), response);
  }

  @Test
  public void getEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.getEvent("1", "1"), response);
  }

  @Test
  public void getAllEventsTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.getAllEvents("1"), response);
  }

  @Test
  public void getEventsByCategoryTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.getEventsByCategory("test", "1"), response);
  }

  @Test
  public void registerTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(stateTest.register("test", "1", "test"), response);
  }

  @Test
  public void deleteAllEventsTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(response, stateTest.deleteAllEvents("1"));
  }

  @Test
  public void deleteEventsByCategoryTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(response, stateTest.deleteEventsByCategory("study","1"));
  }

  @Test
  public void remindFriendTest() {
    LineUser userTest = new LineUser("1","1","1");
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(response, stateTest.remindFriend(userTest, "aloha", "janlup"));
  }

  @Test
  public void profileTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
    assertEquals(response, stateTest.profile("1"));
  }

  @Test
  public void getTimeTest() {
    String test = stateTest.getTime(new String[] {"2020-05-05", "17:30:30"});
    assertEquals(test, "2020-05-05 17:30:30");
  }

  @Test
  public void toStringTest() {
    assertEquals("InputEventTimeState", stateTest.toString());
  }

}