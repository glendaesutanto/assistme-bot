package com.finalproject.assistme.core.flexmessage;

import com.wrapper.spotify.model_objects.specification.Track;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TrackReplyMessageTest {

  @Test
  public void testGetMessageMethod() {
    TrackReplyMessage message = new TrackReplyMessage();
    assertEquals("assistme Bot sent you list of tracks. Please choose one of them:",
        message.get(new ArrayList<Track>()).getText());

  }
}
