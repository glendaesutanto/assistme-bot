package com.finalproject.assistme.core.flexmessage;

import static java.util.Arrays.asList;

import com.finalproject.assistme.model.Event;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.BubbleStyles;
import com.linecorp.bot.model.message.flex.container.BubbleStyles.BlockStyle;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class EventListCard {

  /**
  * Get message to show event list.
  * @param listEvent list of events
  * @param isAll boolean that show that if this is sorted by category of all events
  * @param category the category of the event (if sorted by category)
  * @param user the owner of the events
  * @return FlexMessage
  */
  public FlexMessage get(List<Event> listEvent, boolean isAll, 
      String category, String user) {

    final Box bodyBlock = createBodyBlock(listEvent, isAll, category, user);

    final Box footerBlock = createFooterBlock();

    final BubbleStyles bubbleStyle = createBubbleStyle();

    final Bubble bubble = 
        Bubble.builder()
            .body(bodyBlock)
            .footer(footerBlock)
            .styles(bubbleStyle)
            .build();

    return new FlexMessage("AssistMe-Bot sent you list of events", bubble);
  }

  private Box createBodyBlock(List<Event> listEvent, boolean isAll,
        String category, String user) {

    final Text headerText = Text.builder()
        .text("EVENTS LIST")
        .size(FlexFontSize.SM)
        .weight(TextWeight.BOLD)
        .color("#c18cdb")
        .build();

    String title = "";
    if (isAll) {
      title = "All Events";
    } else {
      title = "Category: " + category;
    }

    final Text titleText = Text.builder()
        .text(title)
        .size(FlexFontSize.Md)
        .wrap(true)
        .weight(TextWeight.BOLD)
        .margin(FlexMarginSize.MD)
        .build();

    final Separator separator = Separator.builder().margin(FlexMarginSize.MD).build();

    final Box eventListBox = createEventListBox(listEvent);

    final Box eventOwnerBox = createEventOwnerBox(user);

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(headerText, titleText, separator, eventListBox, separator, eventOwnerBox))
        .build();
  }

  private Box createEventOwnerBox(String user) {

    final Text eventOwnerTextLabel = Text.builder()
        .text("Owned by")
        .color("#aaaaaa")
        .size(FlexFontSize.XS)
        .align(FlexAlign.START)
        .build();

    final Text eventOwnerTextContent = Text.builder()
        .text(user)
        .color("#aaaaaa")
        .size(FlexFontSize.XS)
        .align(FlexAlign.END)
        .build();

    return Box.builder()
        .layout(FlexLayout.HORIZONTAL)
        .contents(asList(eventOwnerTextLabel, eventOwnerTextContent))
        .margin(FlexMarginSize.MD)
        .build();
  }

  private Box createEventListBox(List<Event> listEvent) {

    List<FlexComponent> eventList = new ArrayList<>();

    final Separator separator = Separator.builder().margin(FlexMarginSize.MD).build();

    for (int i = 0; i < listEvent.size(); i++) {
      Event eventNow = listEvent.get(i);
      String eventNameNow = eventNow.getName();
      String eventCategoryNow = eventNow.getCategory();
      Date eventDueDateNow = eventNow.getDueDateTime();
      Long eventIdNow = eventNow.getEventId();

      Box eventInfoBox = createEventInfoBox(eventNameNow, eventCategoryNow, 
          eventDueDateNow, eventIdNow);

      if (i > 0) {
        eventList.add(separator);
      }
      eventList.add(eventInfoBox);
    }

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .margin(FlexMarginSize.MD)
        .contents(eventList)
        .build();
  }

  private Box createEventInfoBox(String name, String category, Date dueDate, Long id) {

    String dueDateAsString = new SimpleDateFormat("dd-MM-yyyy").format(dueDate);

    final Text eventNameText = Text.builder()
        .text(name)
        .weight(TextWeight.BOLD)
        .size(FlexFontSize.SM)
        .wrap(true)
        .action(new MessageAction(name, "/getEvent " + id))
        .build();

    final Text eventCategoryText = Text.builder()
        .text("Category: " + category)
        .size(FlexFontSize.XS)
        .build();

    final Text eventDueDateText = Text.builder()
        .text("Due date: " + dueDateAsString)
        .size(FlexFontSize.XS)
        .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .margin(FlexMarginSize.MD)
        .contents(asList(eventNameText, eventCategoryText, eventDueDateText))
        .build();
  }

  private Box createFooterBlock() {

    final Text footerText = Text.builder()
        .text("P.S. Click the name of the event to get the details about the event!")
        .size(FlexFontSize.XS)
        .wrap(true)
        .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(footerText))
        .build();
  }

  private BubbleStyles createBubbleStyle() {

    return BubbleStyles.builder()
      .footer(BlockStyle.builder()
              .separator(true).build())
      .build();
  }
}