# AssistMe

A Spring Boot project to fulfill Advanced Programming Course final assignment.

## Authors :
**[Adrian Wijaya](https://gitlab.com/adrian.wijaya)**
1806205363   
[![pipeline status](https://gitlab.com/glendaesutanto/assistme-bot/badges/playList/pipeline.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/Adrian_Spotify) [![coverage report](https://gitlab.com/glendaesutanto/assistme-bot/badges/playList/coverage.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/playList)


**[Glenda Emanuella Sutanto](https://gitlab.com/glendaesutanto)**
1806133774

[![pipeline status](https://gitlab.com/glendaesutanto/assistme-bot/badges/glenda/pipeline.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/glenda) [![coverage report](https://gitlab.com/glendaesutanto/assistme-bot/badges/glenda/coverage.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/glenda)

**[Shella Gabriella](https://gitlab.com/shella_20)**
1806205350

[![pipeline status](https://gitlab.com/glendaesutanto/assistme-bot/badges/shella/pipeline.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/shella) [![coverage report](https://gitlab.com/glendaesutanto/assistme-bot/badges/shella/coverage.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/shella)

**[Tolhas Parulian Jonathan](https://gitlab.com/tolhas.parulian)**
1806141473

[![pipeline status](https://gitlab.com/glendaesutanto/assistme-bot/badges/tolhas_database/pipeline.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/tolhas_database) [![coverage report](https://gitlab.com/glendaesutanto/assistme-bot/badges/tolhas_database/coverage.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/tolhas_database)

**[Brian Athallah Yudiva](https://gitlab.com/brianyudiva)**
1806205804

[![pipeline status](https://gitlab.com/glendaesutanto/assistme-bot/badges/brian/pipeline.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/brian) [![coverage report](https://gitlab.com/glendaesutanto/assistme-bot/badges/brian/coverage.svg)](https://gitlab.com/glendaesutanto/assistme-bot/-/commits/brian)

#### Advanced Programming - B
