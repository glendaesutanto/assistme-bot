package com.finalproject.assistme.handler;

import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UniversalHandler implements Handler {

  private List<Message> responses;

  public UniversalHandler() {
    this.responses = new ArrayList<>();
  }

  public void setResponse(List<Message> responses) {
    this.responses = responses;
  }

  @Override
  public List<Message> getResponse() {
    return this.responses;
  }

}