package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.LineUser;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class UnregisteredState extends State {
  public static final String DB_COL_NAME = "UNREGISTERED";

  public UnregisteredState() {
    this.responses = new ArrayList<>();
  }

  /**
   * Method for making event.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> createEvent(String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Method for cancelling event.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> cancelEvent(String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Delete event.
   * @param eventId event ID.
   * @param userId used ID.
   * @return list of response messages.
   */
  public List<Message> deleteEvent(String eventId, String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Method for other response.
   * @param command list of commands.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> others(String[] command, String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Report mood.
   *
   * @return list of response message
   */
  public List<Message> reportMood() {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Process mood.
   *
   * @param moodType type of the mood
   * @return list of response message
   */
  public List<Message> processMood(String moodType) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Help command is accessed by user.
   *
   * @return list of response message
   */
  public List<Message> help() {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Get an event.
   *
   * @param eventId id of event
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEvent(String eventId, String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Get all events based on userId.
   *
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getAllEvents(String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Get some events based on userId & event category.
   *
   * @param category event category
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEventsByCategory(String category, String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Register user.
   *
   * @param userName user name for AssistMe bot.
   * @param userId user id of user that will register to AssistMe bot
   * @param displayName display name of user that will register to AssistMe bot
   * @return list of response message
   */
  public List<Message> register(String userName, String userId, String displayName) {
    responses.clear();
    try {
      if (userId == null) {
        responses.add(Messages.REGISTRATION_ERROR_FETCHING_USERID);
        return responses;
      } else if (!Pattern.matches("^\\w+$", userName)
              || userName.length() > 255
              || lineUserRepository.isUserNameNotUnique(userName)) {
        responses.add(Messages.USERNAME_SPEC);
        return responses;
      }
      LineUser newUser = new LineUser(userId, userName, displayName);
      newUser.setState(IdleState.DB_COL_NAME);
      lineUserRepository.save(newUser);
      responses.add(Messages.registerSuccess(userName));
      return responses;
    } catch (Exception e) {
      responses.add(Messages.REGISTRATION_ERROR);
      return responses;
    }
  }

  /**
   * Delete all events.
   * @param userId userId from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteAllEvents(String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Delete events by the category.
   * @param category event category
   * @param userId user id from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteEventsByCategory(String category, String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Remind a friend who is a user of AssistMe Bot.
   * @param userFrom user who sends the reminder
   * @param userNameTo username who will receive the reminder
   */
  public List<Message> remindFriend(
      LineUser userFrom,
      String userNameTo,
      String reminderMessage
  ) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }

  /**
   * Get user profile.
   * @param userId user id.
   */
  public List<Message> profile(String userId) {
    responses.clear();
    responses.add(Messages.USER_NOT_REGISTERED);
    responses.add(Messages.USERNAME_SPEC);
    return responses;
  }


  @Override
  public String toString() {
    return "UnregisteredState";
  }

}
