package com.finalproject.assistme.core.flexmessage;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;
import com.wrapper.spotify.model_objects.specification.Track;
import java.util.ArrayList;
import java.util.List;

public class TrackReplyMessage {

  /**
   * Show quickreply message to choose the tracks.
   * @param tracks song that choosen to run
   * @return quickreply message and text message for description
   */
  public TextMessage get(ArrayList<Track> tracks) {
    final List<QuickReplyItem> items = new ArrayList<>();
    for (Track track : tracks) {
      int length = track.getName().length();
      items.add(
          QuickReplyItem.builder()
              .action(new MessageAction(track.getName().substring(0, Math.min(20, length)),
                  "/music " + track.getId()))
              .build()
      );
    }

    final QuickReply quickReply = QuickReply.items(items);

    return TextMessage
        .builder()
        .text("assistme Bot sent you list of tracks. Please choose one of them:")
        .quickReply(quickReply)
        .build();
  }
}