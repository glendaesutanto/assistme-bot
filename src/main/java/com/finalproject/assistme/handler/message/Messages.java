package com.finalproject.assistme.handler.message;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class Messages {

  public static final Message HELP_TEXT = new TextMessage(
      "List of available commands \n"
      + "/help - Show all valid commands \n"
      + "/profile - show user profile \n"
      + "/reportMood - Report your mood \n"
      + "/createEvent - Make your Event \n"
      + "/getAllEvents - List all your events \n"
      + "/getEvent [eventId] - Get the detail of your event with ID eventId \n"
      + "/deleteEvent [eventId] - Delete your event with ID eventId \n"
      + "/getEventsByCategory [category name] - List events by category \n"
      + "/deleteAllEvents - [WARNING] delete all events \n"
      + "/deleteEventsByCategory [category name] - [WARNING] delete events by category\n"
      + "/music [trackId] - Show a music link from spotify URL previewer \n"
      + "/playlist [query] - Find Spotify playlist based on search query \n"
      + "/mood [mood type] - Show image,quotes and playlist button based on mood type \n"
      + "/showTracks [playlistId] - Show (max 5) playlist track available based on playlistId \n"
      + "/remindFriend [your friend username] [your message] - Remind your friend who is "
      + "also a user of AssistMe Bot"
  );

  public static final Message OTHERS_TEXT = new TextMessage(
      "Invalid command! Please type '/help' to show all valid commands"
  );

  public static final Message ERROR_OCCURED = new TextMessage(
      "Unknown error occured"
  );

  public static final Message EVENT_NOT_FOUND = new TextMessage(
          "No event found"
  );

  public static final Message ERROR_WHILE_FETCHING_EVENT = new TextMessage(
          "Error fetching event(s)"
  );

  public static final Message EVENT_NEW_ADD_SUCCESS = new TextMessage(
          "Please enter the name of the event,\ne.g: study\nor type /cancel"
  );

  public static final Message EVENT_ADD_NAME_SUCCESS = new TextMessage(
          "Please enter category for the event\nor type /cancel"
  );

  public static final Message EVENT_ADD_CATEGORY_SUCCESS = new TextMessage(
          "Please enter the due date in format: yyyy-MM-dd HH:mm:ss. (GMT+7)\n"
                  + "due date must not be earlier than today\n"
                  + "e.g: 2030-02-14 17:30:00\nor type /cancel"
  );

  public static final Message EVENT_ADD_DUE_DATE_SUCCESS = new TextMessage(
          "Enter event reminder in format: yyyy-MM-dd HH:mm:ss [message: optional]. (GMT+7)\n"
                  + "Reminder must between today and due date\n"
                  + "e.g: 2030-02-14 17:30:00 Remind Me!\n"
                  + "or type /cancel\nor type 'no' (no reminder)"

  );

  public static final Message EVENT_SUCCESS = new TextMessage("Event done!");

  public static final Message CANCEL_NOT = new TextMessage(
          "Invalid command! You are not in the middle of making event"
  );

  public static final Message CANCEL_SUCCESS = new TextMessage(
          "Adding event cancelled"
  );

  public static final Message REMINDER_WARNING_MESSAGE = new TextMessage(
      "Our bot will be sleeping from 11 PM - 5 AM everyday. (GMT+7)\n"
          + "For your best experience, DO NOT make events at that period of time.\n"
          + "We also recommend you to NOT set reminder between/after that period of time.\n\n"
          + "For example, event created at day X 8PM, reminder setted at day X+1 9AM.\n"
          + "We can't guarantee you our reminder will work because all of the processes "
          + "will be stopped at that period of time.\n\n"
          + "Thank you.\n"
          + "- AssistMe Bot developer team"
  );

  public static final Message USER_NOT_REGISTERED = new TextMessage(
          "You're not registered yet, type /register [name]"
  );

  public static final Message USERNAME_SPEC = new TextMessage("Username must unique, have "
          + "length 1-255, and contains numerical, character, and underscore only"
  );

  public static final Message REGISTRATION_ERROR_FETCHING_USERID = new TextMessage(
          "Line user id not valid"
  );

  public static final Message REGISTRATION_ERROR = new TextMessage("Registration Error");

  public static final Message USER_ALREADY_REGISTERED = new TextMessage(
          "You are already registered"
  );

  public static final Message DELETE_ALL_EVENTS_SUCCESS = new TextMessage(
          "All events successfully deleted"
  );

  public static final Message USER_NOT_FOUND = new TextMessage(
          "User not found"
  );

  /**
   * String maker for event delete name.
   * @param name deleted event name.
   * @return response message.
   */
  public static final Message eventDeleteSuccess(String name) {
    return new TextMessage(
        "Event " + name + " successfully deleted"
    );
  }

  /**
   * String maker for a register that success.
   * @param userName user name from user at AssistMe.
   * @return response message.
   */
  public static final Message registerSuccess(String userName) {
    return new TextMessage(
            userName + " registered. Welcome to AssistMe bot! type /help to see the available "
                    + "commands"
    );
  }

  /**
   * String maker for delete event(s) by category response.
   * @param category category from event(s) to delete.
   * @return response message.
   */
  public static final Message deleteEventsByCategorySuccess(String category) {
    return new TextMessage(
            "Event(s) with category " + category + " successfully deleted"
    );
  }

  /**
   * String maker for remind to a friend.
   * @param userTo user who will receive the reminder
   * @return response message.
   */
  public static final Message remindFriendSuccess(String userTo) {
    return new TextMessage(
            "Successfully send the reminder to " + userTo
    );
  }

  /**
   * String maker for remind to a friend.
   * @param category event category
   * @return response message.
   */
  public static final Message noEventWithProvidedCategory(String category) {
    return new TextMessage(
            "There's no event with " + category + " category"
    );
  }

}