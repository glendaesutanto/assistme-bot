package com.finalproject.assistme.core.flexmessage;

import static java.util.Arrays.asList;

import com.finalproject.assistme.model.LineUser;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;


public class FriendReminderCard {

  /**
   * Reminder card flex message.
   * @param lineUserFrom user who sends the reminder
   * @param lineUserTo user who will receive the reminder
   * @param reminderMessage reminder message
   * @return FlexMessage
   */
  public FlexMessage get(LineUser lineUserFrom, LineUser lineUserTo, String reminderMessage) {

    String userDisplayNameFrom = lineUserFrom.getDisplayName();
    String userDisplayNameTo = lineUserTo.getDisplayName();

    final Box bodyBlock = createBodyBlock(
        userDisplayNameFrom, 
        userDisplayNameTo, 
        reminderMessage
    );

    final Bubble bubble =
        Bubble.builder()
            .body(bodyBlock)
            .build();

    return new FlexMessage("Your friend sent you a reminder!", bubble);
  }

  private Box createBodyBlock(
      String userDisplayNameFrom,
      String userDisplayNameTo,
      String reminderMessage
  ) {

    final Text headerText = Text.builder()
        .text(String.format("HELLO, %s", userDisplayNameTo.toUpperCase()))
        .size(FlexFontSize.SM)
        .color("#c18cdb")
        .weight(TextWeight.BOLD)
        .wrap(true)
        .build();

    final Separator separator = Separator.builder().margin(FlexMarginSize.MD).build();

    final Text title1Text = Text.builder()
        .text("You have a new reminder!")
        .margin(FlexMarginSize.MD)
        .align(FlexAlign.CENTER)
        .weight(TextWeight.BOLD)
        .size(FlexFontSize.Md)
        .build();

    final Text title2Text = Text.builder()
        .text(String.format("From %s", userDisplayNameFrom))
        .margin(FlexMarginSize.MD)
        .size(FlexFontSize.SM)
        .align(FlexAlign.CENTER)
        .weight(TextWeight.BOLD)
        .build();  

    final Text messageLabelText = Text.builder()
        .text("Message")
        .size(FlexFontSize.SM)
        .weight(TextWeight.BOLD)
        .margin(FlexMarginSize.MD)
        .build();

    final Text reminderMessageText = Text.builder()
        .text(reminderMessage)
        .margin(FlexMarginSize.MD)
        .size(FlexFontSize.SM)
        .wrap(true)
        .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(headerText, separator, title1Text, title2Text, separator,
            messageLabelText, reminderMessageText))
        .build();
  }

}