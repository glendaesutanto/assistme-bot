package com.finalproject.assistme.core.mood;

import com.finalproject.assistme.core.present.Image;
import com.finalproject.assistme.core.present.Present;
import com.finalproject.assistme.core.present.Quote;
import com.finalproject.assistme.core.present.SpotifyPlaylist;
import java.util.Random;

public abstract class Mood {
  
  Random generator = new Random();
  protected String[] imageUrlList;
  protected String[] quoteList;

  abstract String getType();

  public Mood(String[] imageUrlList, String[] quoteList) {
    this.imageUrlList = imageUrlList;
    this.quoteList = quoteList;
  }
  
  /**
   * Process method to get the image, quote, and spotify playlist.
   * @return
   */
  public Present process() {
    Image img = this.getImage();
    Quote quote = this.getQuote();
    SpotifyPlaylist spotifyPlaylist = this.getSpotifyPlaylist();
    return new Present(img, quote, spotifyPlaylist);
  }

  abstract Image getImage();

  abstract Quote getQuote();

  abstract SpotifyPlaylist getSpotifyPlaylist();

  String[] getPresentListByType(String presentType) {
    String[] listType;
    if (presentType.equalsIgnoreCase("image")) {
      listType = this.imageUrlList;
    } else {
      listType = this.quoteList;
    }
    return listType;
  }

  String getRandomPresentByMood(String presentType) {
    String[] presentList = getPresentListByType(presentType);
    int randomNumber = generator.nextInt(presentList.length);
    return presentList[randomNumber];
  }
  
}
