package com.finalproject.assistme.handler;

import com.linecorp.bot.model.message.Message;
import java.util.List;

public interface Handler {

  List<Message> getResponse();
}