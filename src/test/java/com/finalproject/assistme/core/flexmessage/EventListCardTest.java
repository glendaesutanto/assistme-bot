package com.finalproject.assistme.core.flexmessage;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EventListCardTest {

  @Test
  public void testGetMessageMethod() {
    LineUser lineUser = new LineUser("userId","userName","displayName");
  	Event event1 = new Event(lineUser,"apa aja","apa aja", new Date());
  	List<Event> listEvent = new ArrayList<>();
  	listEvent.add(event1);
    listEvent.add(event1);
    EventListCard eventListCard = new EventListCard();
    assertEquals("AssistMe-Bot sent you list of events", 
    	eventListCard.get(listEvent,  true, 
    		"apa aja", "glenda").getAltText());
    assertEquals("AssistMe-Bot sent you list of events", 
    	eventListCard.get(listEvent,  false, 
    		"apa aja", "glenda").getAltText());
  }
}
