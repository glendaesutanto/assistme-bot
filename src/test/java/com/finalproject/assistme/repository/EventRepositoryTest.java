package com.finalproject.assistme.repository;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventRepositoryTest{

   @Autowired
   private TestEntityManager entityManager;

   @Autowired
   private EventRepository eventRepository;

   @Mock
   private EventRepository eventRepositoryMock;

   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

   @Test
   public void testFindEventsByUserIdThenReturnListEvent() {
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("1", "agus", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "ngerjain pr", "tugas", dueDateTime);
         entityManager.persist(event);
         entityManager.flush();
         List<Event> eventFound = eventRepository.findEventsByUserId(lineUser.getUserId());
         assertThat(eventFound.get(0).getEventId())
               .isEqualTo(event.getEventId());
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testFindEventsByUserIdAsyncThenReturnListEvent() {
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("1", "agus", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "ngerjain pr", "tugas", dueDateTime);
         entityManager.persist(event);
         entityManager.flush();
         CompletableFuture<List<Event>> eventFound = eventRepositoryMock.findEventsByUserIdAsync(lineUser.getUserId());
         verify(eventRepositoryMock, times(1)).findEventsByUserIdAsync(lineUser.getUserId());
      } catch (ParseException e) {
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testFindEventByEventIdThenReturnEvent() {
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("2", "baim", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "belajar matematika", "belajar", dueDateTime);
         entityManager.persist(event);
         entityManager.flush();
         Event eventFound = eventRepository.findEventByEventId(event.getEventId(), lineUser.getUserId());
         assertThat(eventFound.getEventId())
                 .isEqualTo(event.getEventId());
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testDeleteEventByEventId() {
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("3", "torang", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "olahraga", "lifestyle", dueDateTime);
         entityManager.persist(event);
         entityManager.flush();
         eventRepository.deleteEventByEventId(event.getEventId(), lineUser.getUserId());
         assertNull(eventRepository.findEventByEventId(event.getEventId(), lineUser.getUserId()));
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testDeleteEventsByUserId() throws ParseException{
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("4", "cia", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "bermain", "refreshing", dueDateTime);
         Event event2 = new Event(lineUser, "belajar", "study", dueDateTime);
         entityManager.persist(event);
         entityManager.persist(event2);
         entityManager.flush();
         eventRepository.deleteEventsByUserId("4");
         assertNull(eventRepository.findEventByEventId(event.getEventId(), lineUser.getUserId()));
         assertNull(eventRepository.findEventByEventId(event2.getEventId(), lineUser.getUserId()));
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testDeleteEventsByCategory() throws ParseException{
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("4", "cia", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "bermain", "refreshing", dueDateTime);
         Event event2 = new Event(lineUser, "berlari", "refreshing", dueDateTime);
         entityManager.persist(event);
         entityManager.persist(event2);
         entityManager.flush();
         eventRepository.deleteEventsByCategory("refreshing", "4");
         assertNull(eventRepository.findEventByEventId(event.getEventId(), lineUser.getUserId()));
         assertNull(eventRepository.findEventByEventId(event2.getEventId(), lineUser.getUserId()));
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testDeleteEventsByCategoryCheckForFullEqual() throws ParseException{
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("4", "cia", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "bermain", "refreshing", dueDateTime);
         Event event2 = new Event(lineUser, "berlari", "refresh", dueDateTime);
         entityManager.persist(event);
         entityManager.persist(event2);
         entityManager.flush();
         eventRepository.deleteEventsByCategory("refreshing", "4");
         assertNull(eventRepository.findEventByEventId(event.getEventId(), lineUser.getUserId()));
         assertNotNull(eventRepository.findEventByEventId(event2.getEventId(), lineUser.getUserId()));
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testfindEventsByNameThenReturnListEvent() {
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("2", "baim", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "belajar matematika", "belajar", dueDateTime);
         entityManager.persist(event);
         entityManager.flush();
         List<Event> eventFound = eventRepository.findEventsByName(event.getName(), lineUser.getUserId());
         assertThat(eventFound.get(0).getName())
                 .isEqualTo(event.getName());
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testFindEventsByCategoryNotEqualThenReturnListEvent() {
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("2", "baim", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "belajar matematika", "belajar", dueDateTime);
         entityManager.persist(event);
         entityManager.flush();
         List<Event> eventFound = eventRepository.findEventsByCategory("lajar", lineUser.getUserId());
         assertTrue(eventFound.isEmpty());
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

   @Test
   public void testfindEventsByNameNotEqualThenReturnListEvent() {
      try{
         Date dueDateTime = format.parse("2030-02-10 01:14:15.000");
         LineUser lineUser = new LineUser("2", "baim", "kuwuk");
         entityManager.persist(lineUser);
         entityManager.flush();
         Event event = new Event(lineUser, "belajar matematika", "belajar", dueDateTime);
         entityManager.persist(event);
         entityManager.flush();
         List<Event> eventFound = eventRepository.findEventsByName("belajar mat", lineUser.getUserId());
         assertTrue(eventFound.isEmpty());
      } catch (ParseException e){
         System.out.println(e.getMessage());
      }
   }

}