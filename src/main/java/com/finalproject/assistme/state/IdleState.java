package com.finalproject.assistme.state;

import com.finalproject.assistme.core.flexmessage.EventCard;
import com.finalproject.assistme.core.flexmessage.EventListCard;
import com.finalproject.assistme.core.flexmessage.FriendReminderCard;
import com.finalproject.assistme.core.flexmessage.MoodReplyMessage;
import com.finalproject.assistme.core.flexmessage.ProfileCard;
import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.state.helper.MoodHelper;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.stereotype.Component;

@Component
public class IdleState extends State {
  public static final String DB_COL_NAME = "IDLE";

  public IdleState() {
    this.responses = new ArrayList<>();
  }

  /**
   * Method for making event.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> createEvent(String userId) {
    responses.clear();
    try {
      LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
      Event event = new Event(lineUser, "placeholder", "placeholder", new Date());
      lineUser.setState(InputEventNameState.DB_COL_NAME);

      eventRepository.save(event);
      lineUserRepository.save(lineUser);

      responses.add(Messages.EVENT_NEW_ADD_SUCCESS);
    } catch (NullPointerException e) {
      responses.add(Messages.ERROR_OCCURED);
    }

    return responses;
  }

  /**
   * Method for cancelling event.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> cancelEvent(String userId) {
    responses.clear();
    responses.add(Messages.CANCEL_NOT);
    return responses;
  }

  /**
   * Delete event.
   * @param eventId event ID.
   * @param userId used ID.
   * @return list of response messages.
   */
  public List<Message> deleteEvent(String eventId, String userId) {
    responses.clear();
    try {
      String eventName = eventRepository.findEventByEventId(
              Long.parseLong(eventId), userId).getName();
      eventRepository.deleteEventByEventId(Long.parseLong(eventId), userId);
      responses.add(Messages.eventDeleteSuccess(eventName));
    } catch (NullPointerException e) {
      responses.add(Messages.EVENT_NOT_FOUND);
    }
    return responses;
  }

  /**
   * Method for other response.
   * @param command list of commands.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> others(String[] command, String userId) {
    responses.clear();
    responses.add(Messages.OTHERS_TEXT);
    return responses;
  }

  /**
   * Report mood.
   *
   * @return list of response message
   */
  public List<Message> reportMood() {
    responses.clear();
    responses.add(new MoodReplyMessage().getMoodReplyMessage());
    return responses;
  }

  /**
   * Process mood.
   *
   * @param moodType type of the mood
   * @return list of response message
   */
  public List<Message> processMood(String moodType) {
    responses.clear();
    MoodHelper moodHelper = new MoodHelper();
    responses = moodHelper.processMood(moodType);
    return responses;
  }

  /**
   * Help command is accessed by user.
   *
   * @return list of response message
   */
  public List<Message> help() {
    responses.clear();
    responses.add(Messages.HELP_TEXT);
    responses.add(Messages.REMINDER_WARNING_MESSAGE);
    return responses;
  }

  /**
   * Get an event.
   *
   * @param eventId id of event
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEvent(String eventId, String userId) {
    responses.clear();
    try {
      LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
      Event event = eventRepository.findEventByEventId(Long.parseLong(eventId), userId);
      if (event == null) {
        responses.add(Messages.EVENT_NOT_FOUND);
        return responses;
      } else {
        FlexMessage flexGetEvent = new EventCard().get(event, lineUser.getDisplayName());
        responses.add(flexGetEvent);
        return responses;
      }
    } catch (Exception e) {
      responses.add(Messages.ERROR_WHILE_FETCHING_EVENT);
      return responses;
    }
  }

  /**
   * Get all events based on userId.
   *
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getAllEvents(String userId) {
    responses.clear();
    try {
      LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
      CompletableFuture<List<Event>> lineUserAsync =
              eventRepository.findEventsByUserIdAsync(userId);
      List<Event> listEvent = lineUserAsync.get();
      if (listEvent.isEmpty()) {
        responses.add(Messages.EVENT_NOT_FOUND);
        return responses;
      } else {
        FlexMessage flexMessageResponse = new EventListCard().get(
                listEvent, true, "general", lineUser.getDisplayName());
        responses.add(flexMessageResponse);
        return responses;
      }
    } catch (Exception e) {
      responses.add(Messages.ERROR_WHILE_FETCHING_EVENT);
      return responses;
    }
  }

  /**
   * Get some events based on userId & event category.
   *
   * @param category event category
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEventsByCategory(String category, String userId) {
    responses.clear();
    try {
      LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
      List<Event> listEventC = eventRepository.findEventsByCategory(category, userId);
      if (category.length() <= 0) {
        responses.add(Messages.OTHERS_TEXT);
        return responses;
      } else if (listEventC.isEmpty()) {
        responses.add(Messages.noEventWithProvidedCategory(category));
        return responses;
      } else {
        FlexMessage flexMessageEC = new EventListCard().get(
                listEventC, false, category, lineUser.getDisplayName());
        responses.add(flexMessageEC);
        return responses;
      }
    } catch (Exception e) {
      responses.add(Messages.ERROR_WHILE_FETCHING_EVENT);
      return responses;
    }
  }

  /**
   * Register user.
   *
   * @param userName user name for AssistMe bot
   * @param userId user id of user that will register to AssistMe bot
   * @param displayName display name of user that will register to AssistMe bot
   * @return list of response message
   */
  public List<Message> register(String userName, String userId, String displayName) {
    responses.clear();
    responses.add(Messages.USER_ALREADY_REGISTERED);
    return responses;
  }

  /**
   * Delete all events.
   * @param userId userId from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteAllEvents(String userId) {
    responses.clear();
    try {
      List<Event> events = eventRepository.findEventsByUserId(userId);
      if (events.isEmpty()) {
        responses.add(Messages.EVENT_NOT_FOUND);
      } else {
        eventRepository.deleteEventsByUserId(userId);
        responses.add(Messages.DELETE_ALL_EVENTS_SUCCESS);
      }
    } catch (Exception e) {
      responses.add(Messages.ERROR_OCCURED);
    }
    return responses;
  }

  /**
   * Delete events by the category.
   * @param category event category
   * @param userId user id from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteEventsByCategory(String category, String userId) {
    responses.clear();
    try {
      List<Event> events = eventRepository.findEventsByCategory(category, userId);
      if (category.length() <= 0) {
        responses.add(Messages.OTHERS_TEXT);
      } else if (events.isEmpty()) {
        responses.add(Messages.noEventWithProvidedCategory(category));
      } else {
        eventRepository.deleteEventsByCategory(category, userId);
        responses.add(Messages.deleteEventsByCategorySuccess(category));
      }
    } catch (Exception e) {
      responses.add(Messages.ERROR_OCCURED);
    }
    return responses;
  }

  /**
   * Remind a friend who is a user of AssistMe Bot.
   * @param userFrom user who sends the reminder
   * @param userNameTo username who will receive the reminder
   */
  public List<Message> remindFriend(
          LineUser userFrom,
          String userNameTo,
          String reminderMessage
  ) {
    responses.clear();
    LineUser userTo = lineUserRepository.findLineUserByUserName(userNameTo);
    if (userTo != null) {
      Message reminderCard = new FriendReminderCard().get(userFrom, userTo, reminderMessage);
      responses.add(Messages.remindFriendSuccess(userTo.getDisplayName()));
      lineMessagingClient.pushMessage(new PushMessage(userTo.getUserId(), reminderCard));
    } else {
      responses.add(Messages.USER_NOT_FOUND);
    }

    return responses;
  }

  /**
   * Get user profile.
   * @param userId user id.
   */
  public List<Message> profile(String userId) {
    responses.clear();
    try {
      LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
      FlexMessage flexProfileCard = new ProfileCard().get(lineUser.getDisplayName(),
              lineUser.getUserName());
      responses.add(flexProfileCard);
      return responses;
    } catch (Exception e) {
      responses.add(Messages.ERROR_OCCURED);
      return responses;
    }
  }

  @Override
  public String toString() {
    return "IdleState";
  }

}