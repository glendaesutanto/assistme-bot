package com.finalproject.assistme.controller;

import com.finalproject.assistme.EventTestUtil;
import com.finalproject.assistme.handler.ApplicationService;
import com.finalproject.assistme.handler.UniversalHandler;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.musicplayer.Spotify;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.finalproject.assistme.state.IdleState;
import com.finalproject.assistme.state.State;
import com.finalproject.assistme.state.helper.StateHelper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AssistMeControllerTest {

  @Spy
  @InjectMocks
  private AssistMeController assistMeController;

  @Mock
  private LineMessagingClient lineMessagingClient;

  @Mock
  private CompletableFuture<UserProfileResponse> usrprcompletableFuture;
  
  @Mock
  private CompletableFuture<BotApiResponse> botCompletableFuture;

  @Mock
  private CompletableFuture<List<Event>> eventCompletableFuture;

  @Mock
  private LineUserRepository lineUserRepository;

  @Mock
  private EventRepository eventRepository;

  @Mock
  private StateHelper stateHelper;

  @Mock
  private State state;

  @Mock
  private UniversalHandler universalHandler;

  @Mock
  private ApplicationService applicationService;

  @Test
  public void contextLoads() throws Exception {
    assertThat(assistMeController).isNotNull();
  }

  @Before
  public void setUp() throws ExecutionException, InterruptedException, ParseException {
    LineUser lineUser = new LineUser("1", "lah", "dummy");
    Event event = new Event(
            (lineUserRepository.findLineUserByUserId("1")),
            "waw",
            "work",
            new Date());
    event.setVefiry(false);
    UserProfileResponse userProfileResponse = new UserProfileResponse(
        "dummy", "1", "file://stub", "hello");
    UserProfileResponse userProfileResponse2 = new UserProfileResponse(
        "dummy", "2", "file://stub", "hello");
    //user '1' for registered user
    when(lineMessagingClient.getProfile("1")).thenReturn(usrprcompletableFuture);
    when(lineMessagingClient.getProfile("1").get()).thenReturn(userProfileResponse);
    //user '2' for unregistered user
    when(lineMessagingClient.getProfile("2")).thenReturn(usrprcompletableFuture);
    when(lineMessagingClient.getProfile("2").get()).thenReturn(userProfileResponse);
    when(lineUserRepository.isLineUserRegistered("1")).thenReturn(true);
    when(lineUserRepository.isLineUserRegistered("2")).thenReturn(false);
    when(stateHelper.getUserState("1")).thenReturn(new IdleState());

    }

  @Test
  public void getMessageTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("halo!","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getAudioMessageTest() throws Exception {
    Spotify.clientCredentialsSync();
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken", new UserSource("1"),
            new TextMessageContent("id","/music music"), Instant.parse("2020-01-01T00:00:00.000Z"));
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getUserDisplayNameTest() throws ExecutionException, InterruptedException {
    assertEquals("dummy", assistMeController.getUserDisplayName("1"));
  }

  @Test
  public void registerLineUserPassedTest() throws ExecutionException, InterruptedException {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/register landi", "1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void moodHappyCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/mood happy","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void moodSadCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/mood sad","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void moodProductiveCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/mood productive","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void moodTiredCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/mood tired","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void moodOtherCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/mood hi","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void testCommandTest() {
    Spotify.clientCredentialsSync();
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken", new UserSource("1"),
        new TextMessageContent("id", "/playlist happy"), Instant.parse("2020-01-01T00:00:00.000Z"));

    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void showTracksCommandTest() {
    Spotify.clientCredentialsSync();
    MessageEvent<TextMessageContent> event;
    event = new MessageEvent<>("replyToken", new UserSource("1"),
        new TextMessageContent("id", "/showTracks 37i9dQZF1DXdPec7aLTmlC"),
        Instant.parse("2020-01-01T00:00:00.000Z"));

    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void createEventTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/createEvent");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void deleteEventTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/deleteEvent 1");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void deleteAllEventsTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/deleteAllEvents");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void deleteEventsByCategoryTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/deleteEventsByCategory 2");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void cancelTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/cancel");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/getEvent 12", "1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getAllEventsTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent("id", "/getAllEvents");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getEventsByCategoryTest() throws Exception {
    MessageEvent<TextMessageContent> event;
    String replyToken = "replyToken";
    UserSource userSource = new UserSource("1");
    TextMessageContent textMessageContent = new TextMessageContent(
            "id", "/getEventsByCategory lifestyle");
    Instant timeStamp = Instant.parse("2020-01-01T00:00:00.000Z");
    event = new MessageEvent<>(replyToken, userSource, textMessageContent, timeStamp);
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getUserStateTest() throws Exception {
    assistMeController.isLineUserRegistered("2");
    assistMeController.isLineUserRegistered("1");
    assertEquals(assistMeController.isLineUserRegistered("1"), true);
    assertEquals(assistMeController.isLineUserRegistered("2"), false);
  }

  @Test
  public void reportCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/reportMood","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void helpCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/help","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

  @Test
  public void getCommandTest() throws Exception {
    String[] input = new String[] {"/mood", "happy", "\n", "yaya"};
    String command = assistMeController.getCommand(input);
    Mockito.verify(assistMeController).getCommand(input);
    assertEquals("/mood", command);
  }

  @Test
  public void getUserInputJoinedTest() throws Exception {
    String[] input = new String[] {"/register", "happy", "\n", "YayA"};
    String command = assistMeController.getUserInputJoined(input);
    Mockito.verify(assistMeController).getUserInputJoined(input);
    assertEquals("happy \n YayA", command);
  }

  @Test
  public void remindFriendCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/remindFriend lah halo","1");
    try {
      assistMeController.handleTextMessageEvent(event);
      Mockito.verify(assistMeController).handleTextMessageEvent(event);
    } catch(Exception e) {

    }
  }

  @Test
  public void profileCommandTest() {
    MessageEvent<TextMessageContent> event;
    event = EventTestUtil.createDummyTextMessage("/profile","1");
    assistMeController.handleTextMessageEvent(event);
    Mockito.verify(assistMeController).handleTextMessageEvent(event);
  }

}