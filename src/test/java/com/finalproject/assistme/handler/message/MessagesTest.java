package com.finalproject.assistme.handler.message;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MessagesTest {
    @Spy
    @InjectMocks
    Messages messages;

    @Test
    public void contextLoads() throws Exception {
        assertThat(messages).isNotNull();
    }

    @Test
    public void registerSuccessResponseThenReturnMessageTest() throws Exception {
        Message halo = messages.registerSuccess("yow");
        assertThat(halo).isEqualTo(
            new TextMessage(
                    "yow registered. Welcome to AssistMe bot! type /help to see the available commands"
            )
        );
    }

    @Test
    public void deleteEventsByCategorySuccessResponseThenReturnMessageTest() throws Exception {
        Message halo = messages.deleteEventsByCategorySuccess("yow");
        assertThat(halo).isEqualTo(
                new TextMessage(
                        "Event(s) with category yow successfully deleted"
                )
        );
    }

    @Test
    public void noEventWithProvidedCategorySuccessResponseThenReturnMessageTest() throws Exception {
        Message halo = messages.noEventWithProvidedCategory("yow");
        assertThat(halo).isEqualTo(
                new TextMessage(
                        "There's no event with yow category"
                )
        );
    }
}