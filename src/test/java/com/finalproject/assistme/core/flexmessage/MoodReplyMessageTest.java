package com.finalproject.assistme.core.flexmessage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoodReplyMessageTest {

  @Test
  public void testGetMessageMethod() {
    MoodReplyMessage message = new MoodReplyMessage();
    assertEquals("What's your mood right now? Please select or reply with '/mood [yourMood]'",
          message.getMoodReplyMessage().getText());
  }
}