package com.finalproject.assistme.core.present;

import com.finalproject.assistme.musicplayer.Spotify;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.Track;
import java.util.ArrayList;

public class SpotifyPlaylist {

  /**
   * Return a Playlist for given search query.
   * @param query keyword to search playlist
   * @return Playlist information that is looking for
   */
  public ArrayList<PlaylistSimplified> getPlaylists(String query) {
    ArrayList<PlaylistSimplified> playlistSimplified;
    playlistSimplified = Spotify.searchPlayList(query);
    return playlistSimplified;
  }

  public ArrayList<Track> getTracks(String playlistId) {
    return Spotify.getPlaylistsTracksSync(playlistId);
  }

}
