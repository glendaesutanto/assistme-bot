package com.finalproject.assistme.repository;

import com.finalproject.assistme.model.Event;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
  @Query(value = "SELECT * FROM EVENT WHERE LINE_USER_USER_ID = ?1", nativeQuery = true)
  List<Event> findEventsByUserId(String userId);

  @Async
  @Query(value = "SELECT * FROM EVENT WHERE LINE_USER_USER_ID = ?1 ORDER BY DUE_DATE_TIME ASC",
          nativeQuery = true)
  public CompletableFuture<List<Event>> findEventsByUserIdAsync(String userId);

  @Query(value = "SELECT * FROM EVENT WHERE EVENT_ID = ?1 AND LINE_USER_USER_ID = ?2",
          nativeQuery = true)
  Event findEventByEventId(Long eventId, String userId);

  @Query(value = "SELECT * FROM EVENT WHERE LINE_USER_USER_ID = ?1 AND verified = FALSE",
          nativeQuery = true)
  Event findUnverifiedEventByUserId(String userId);

  @Modifying
  @Transactional
  @Query(value = "DELETE FROM EVENT WHERE EVENT_ID = ?1 AND LINE_USER_USER_ID = ?2",
          nativeQuery = true)
  void deleteEventByEventId(Long eventId, String userId);

  @Modifying
  @Transactional
  @Query(value = "DELETE FROM EVENT WHERE LINE_USER_USER_ID = ?1", nativeQuery = true)
  void deleteEventsByUserId(String userId);

  @Modifying
  @Transactional
  @Query(value = "DELETE FROM EVENT WHERE CATEGORY ILIKE ?1 AND LINE_USER_USER_ID = ?2",
          nativeQuery = true)
  void deleteEventsByCategory(String category, String userId);

  @Query(
          value =
          "SELECT * FROM EVENT WHERE NAME ILIKE ?1 AND LINE_USER_USER_ID = ?2",
          nativeQuery = true)
  List<Event> findEventsByName(String name, String userId);

  @Query(
          value = "SELECT * FROM EVENT WHERE CATEGORY ILIKE ?1 AND LINE_USER_USER_ID = ?2",
          nativeQuery = true)
  List<Event> findEventsByCategory(String category, String userId);
}

