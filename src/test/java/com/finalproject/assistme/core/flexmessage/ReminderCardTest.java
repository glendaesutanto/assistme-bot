package com.finalproject.assistme.core.flexmessage;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReminderCardTest {

	@Test
	public void testGetMessageMethod() {
		LineUser lineUser = new LineUser("userId","userName","displayName");
		Event event = new Event(lineUser,"apa aja","apa aja", new Date());
    	event.setReminderTime(new Date());
    	ReminderCard reminderCard = new ReminderCard();
		assertEquals("AssistMe-Bot sent you a reminder!",
      		reminderCard.get(event, "janlup").getAltText());
	}
}