package com.finalproject.assistme.reminder;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ReminderTaskTest {

	@Mock
	LineMessagingClient lineMessagingClient;

	@Mock
  	EventRepository eventRepository;

  	@Mock
	private LineUserRepository lineUserRepository;

	private ReminderTask reminderTask;

	@BeforeEach
	public void setUp() {
		LineUser lineUser = new LineUser("userId","userName","displayName");
  		Event event = new Event(lineUser,"apa aja","apa aja", new Date());
  		event.setReminderTime(new Date());
  		Reminder reminder = new Reminder(event, "janlup");
  		reminderTask = new ReminderTask(lineMessagingClient, eventRepository);
  		reminderTask.setReminder(reminder);
  		reminderTask.doRemind();
	}

	@Test
	public void testNotNull() {
		assertNotNull(reminderTask);
	}
}