package com.finalproject.assistme.reminder.helper;

import com.linecorp.bot.model.message.FlexMessage;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class ScheduledCompletable {

  /**
   * Implement scheduled task asynchronously with completable future.
   * @param executor as scheduled executor service
   * @param command as return object
   * @param delay as delay time
   * @param unit TimeUnit used
   * @return CompletableFuture< FlexMessage > 
   */
  public static CompletableFuture<FlexMessage> scheduleAsync(
      ScheduledExecutorService executor,
      Supplier<CompletableFuture<FlexMessage>> command,
      long delay,
      TimeUnit unit
  ) {
    CompletableFuture<FlexMessage> completableFuture = new CompletableFuture<>();
    executor.schedule(
        (() -> {
            command.get().thenAccept(
                t -> {
                    completableFuture.complete(t);
                }
            )
                .exceptionally(
                    t -> {
                        completableFuture.completeExceptionally(t);
                        return null;
                    }
            );
        }),
        delay,
        unit
    );
    return completableFuture;
  }

}