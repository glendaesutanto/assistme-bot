package com.finalproject.assistme.core.mood;

import com.finalproject.assistme.core.present.Image;
import com.finalproject.assistme.core.present.ImageUrlList;
import com.finalproject.assistme.core.present.Quote;
import com.finalproject.assistme.core.present.QuoteList;
import com.finalproject.assistme.core.present.SpotifyPlaylist;
import java.util.Random;

public class OtherMood extends Mood {

  Random generator = new Random();

  public OtherMood() {
    super(ImageUrlList.otherList, QuoteList.otherList);
  }
  
  String getType() {
    return "Other";
  }

  Image getImage() {
    String url = getRandomPresentByMood("image");
    return new Image(url);
  }

  Quote getQuote() {
    String message = getRandomPresentByMood("quote");
    return new Quote(message);
  }

  SpotifyPlaylist getSpotifyPlaylist() {
    return new SpotifyPlaylist();
  }
  
}
