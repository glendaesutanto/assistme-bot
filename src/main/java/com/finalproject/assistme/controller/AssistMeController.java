package com.finalproject.assistme.controller;

import com.finalproject.assistme.core.flexmessage.PlaylistMessage;
import com.finalproject.assistme.core.flexmessage.TrackReplyMessage;
import com.finalproject.assistme.core.mood.MoodFactory;
import com.finalproject.assistme.core.present.SpotifyPlaylist;
import com.finalproject.assistme.handler.ApplicationService;
import com.finalproject.assistme.handler.UniversalHandler;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.musicplayer.Spotify;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.finalproject.assistme.state.State;
import com.finalproject.assistme.state.helper.StateHelper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.AudioMessage;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.Track;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

@LineMessageHandler
public class AssistMeController {

  private MoodFactory factory = new MoodFactory();

  @Autowired
  private LineMessagingClient lineMessagingClient;

  @Autowired
  private LineUserRepository lineUserRepository;

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private StateHelper stateHelper;

  @Autowired
  private UniversalHandler universalHandler;

  @Autowired
  private ApplicationService applicationService;

  /**
   * Text Message Handler.
   *
   * @param event represent message event that occurs.
   */
  @EventMapping
  public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
    String replyToken = event.getReplyToken();
    final String originalMessageText = event.getMessage().getText();
    String[] splittedMessage = originalMessageText.trim().split(" ");
    String command = getCommand(splittedMessage);
    Source source = event.getSource();
    String userId = source.getUserId();
    String displayName = getUserDisplayName(userId);

    List<Message> responses = new ArrayList<>();

    State state = stateHelper.getUserState(userId);
    switch (command) {
      case "/register":
        String userName = getUserInputJoined(splittedMessage);
        universalHandler.setResponse(state.register(userName, userId, displayName));
        applicationService.setHandler(universalHandler);
        break;

      case "/help":
        universalHandler.setResponse(state.help());
        applicationService.setHandler(universalHandler);
        break;

      case "/music":
        List<Message> musicMessage = new ArrayList<>();
        String trackId = splittedMessage[1];
        String trackUrl = Spotify.getTrackSync(trackId);

        if (trackUrl == null) {
          TextMessage response = new TextMessage(
                  "Error: This track doesn't have URL previewer.\n"
                          + "Consider to listen directly to spotify.");
          musicMessage.add(response);
        } else {
          AudioMessage audioMessageResponse = new AudioMessage(Spotify.getTrackSync(trackId),
                  30000);
          musicMessage.add(audioMessageResponse);
        }
        universalHandler.setResponse(musicMessage);
        applicationService.setHandler(universalHandler);
        break;

      case "/mood":
        String moodType = this.getMessage(subArray(splittedMessage, 1, splittedMessage.length));
        universalHandler.setResponse(state.processMood(moodType));
        applicationService.setHandler(universalHandler);
        break;

      case "/playlist":
        List<Message> playListMessage = new ArrayList<>();
        String query = this.getMessage(subArray(splittedMessage, 1, splittedMessage.length));
        try {
          SpotifyPlaylist playLists = new SpotifyPlaylist();
          ArrayList<PlaylistSimplified> playListArr = playLists.getPlaylists(query);
          PlaylistSimplified playList = playListArr.get(0);

          com.wrapper.spotify.model_objects.specification.Image img = null;
          for (com.wrapper.spotify.model_objects.specification.Image image : playList
                  .getImages()) {
            img = image;
            break;
          }

          FlexMessage response = new PlaylistMessage()
                  .get(img.getUrl(), playList.getExternalUrls().get("spotify"), playList.getName(),
                          playList.getName() + " Playlist", "/showTracks " + playList.getId());
          playListMessage.add(response);
        } catch (NullPointerException e) {
          TextMessage response = new TextMessage("Error: Playlist Image doesn't exists");
          playListMessage.add(response);
        } catch (IndexOutOfBoundsException e) {
          TextMessage response = new TextMessage("Error: Such playlist doesn't exists.");
          playListMessage.add(response);
        } catch (Exception e) {
          TextMessage response = new TextMessage("Error: Undefined behaviour.");
          playListMessage.add(response);
        }
        universalHandler.setResponse(playListMessage);
        applicationService.setHandler(universalHandler);
        break;

      case "/showTracks":
        List<Message> showTrackMessages = new ArrayList<>();
        String playListId = splittedMessage[1];
        SpotifyPlaylist playLists = new SpotifyPlaylist();
        ArrayList<Track> tracks = playLists.getTracks(playListId);
        Message trackReplyMessageResponse = new TrackReplyMessage().get(tracks);
        showTrackMessages.add(trackReplyMessageResponse);
        universalHandler.setResponse(showTrackMessages);
        applicationService.setHandler(universalHandler);
        break;

      case "/reportMood":
        universalHandler.setResponse(state.reportMood());
        applicationService.setHandler(universalHandler);
        break;

      case "/createEvent":
        universalHandler.setResponse(state.createEvent(userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/deleteAllEvents":
        universalHandler.setResponse(state.deleteAllEvents(userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/deleteEventsByCategory":
        String category = getUserInputJoined(splittedMessage);
        universalHandler.setResponse(state.deleteEventsByCategory(category, userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/deleteEvent":
        universalHandler.setResponse(state.deleteEvent(splittedMessage[1], userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/cancel":
        universalHandler.setResponse(state.cancelEvent(userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/getEvent":
        universalHandler.setResponse(state.getEvent(splittedMessage[1], userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/getAllEvents":
        universalHandler.setResponse(state.getAllEvents(userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/getEventsByCategory":
        category = getUserInputJoined(splittedMessage);
        universalHandler.setResponse(state.getEventsByCategory(category, userId));
        applicationService.setHandler(universalHandler);
        break;

      case "/remindFriend":
        String userNameTo = splittedMessage[1];
        String reminderMessage;
        if (splittedMessage.length > 2) {
          reminderMessage = this.getMessage(subArray(splittedMessage, 2, splittedMessage.length));
        } else {
          reminderMessage = "Hello from your friend!";
        }
        LineUser userFrom = lineUserRepository.findLineUserByUserId(userId);
        universalHandler.setResponse(state.remindFriend(userFrom, userNameTo, reminderMessage));
        applicationService.setHandler(universalHandler);
        break;

      case "/profile":
        universalHandler.setResponse(state.profile(userId));
        applicationService.setHandler(universalHandler);
        break;

      default:
        universalHandler.setResponse(state.others(splittedMessage, userId));
        applicationService.setHandler(universalHandler);
        break;
    }
    List<Message> responseMessage = applicationService.getResponse();
    this.reply(replyToken, responseMessage);
  }

  private void reply(String replyToken, List<Message> message) {
    try {
      lineMessagingClient.replyMessage(
              new ReplyMessage(replyToken, message)).get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    } catch (NullPointerException e) {
      System.out.println(e);
    }
  }

  /**
   * Get user display.
   *
   * @param userId represent userId from lineUser.
   */
  public String getUserDisplayName(String userId) {
    try {
      return lineMessagingClient.getProfile(userId).get().getDisplayName();
    } catch (ExecutionException | InterruptedException e) {
      return e.getMessage();
    } catch (NullPointerException e) {
      return e.getMessage();
    }
  }

  /**
   * Get user display.
   *
   * @param userId represent userId from lineUser.
   */
  public boolean isLineUserRegistered(String userId) {
    if (lineUserRepository.isLineUserRegistered(userId)) {
      return true;
    } else {
      return false;
    }
  }

  public String getMessage(String[] arrMessage) {
    String joinedMessage = String.join(" ", arrMessage);
    return joinedMessage;
  }

  public String getCommand(String[] splittedMessage) {
    return splittedMessage[0];
  }

  public String getUserInputJoined(String[] splittedMessage) {
    String[] nameSplitted = subArray(splittedMessage, 1, splittedMessage.length);
    return getMessage(nameSplitted);
  }

  //references: https://www.techiedelight.com/get-subarray-array-specified-indexes-java/
  public <T> T[] subArray(T[] array, int start, int end) {
    return Arrays.copyOfRange(array, start, end);
  }
}
