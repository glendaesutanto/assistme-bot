package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.linecorp.bot.model.message.Message;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class InputCategoryState extends State {
  public static final String DB_COL_NAME = "INPUT_CATEGORY";

  public InputCategoryState() {
    this.responses = new ArrayList<>();
  }

  /**
   * Method for creating event at this state.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> createEvent(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Cancel event making.
   * @param userId user ID.
   * @return list of response message.
   */
  public List<Message> cancelEvent(String userId) {
    responses.clear();
    LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
    Event event = eventRepository.findUnverifiedEventByUserId(userId);

    lineUser.setState(IdleState.DB_COL_NAME);
    lineUserRepository.save(lineUser);
    eventRepository.deleteEventByEventId(event.getEventId(), userId);

    responses.add(Messages.CANCEL_SUCCESS);
    return responses;
  }

  /**
   * Method for deleting event at this state.
   * @param eventId event ID.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> deleteEvent(String eventId, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Category process for this state.
   * @param message Category name.
   * @param userId User ID.
   * @return list of response message.
   */
  public List<Message> others(String[] message, String userId) {
    responses.clear();
    try {
      String eventCategory = message[0];
      Event event = eventRepository.findUnverifiedEventByUserId(userId);
      event.setCategory(eventCategory);

      LineUser user = event.getUser();
      user.setState(InputEventTimeState.DB_COL_NAME);
    
      eventRepository.save(event);
      lineUserRepository.save(user);

      responses.add(Messages.EVENT_ADD_CATEGORY_SUCCESS);
      responses.add(Messages.REMINDER_WARNING_MESSAGE);
    } catch (NullPointerException e) {
      responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    }
    return responses;

  }

  /**
   * Report mood.
   * @return list of response message
   */
  public List<Message> reportMood() {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Process mood.
   * @param moodType type of the mood
   * @return list of response message
   */
  public List<Message> processMood(String moodType) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Help command is accessed by user.
   * @return list of response message
   */
  public List<Message> help() {
    responses.clear();
    responses.add(Messages.HELP_TEXT);
    return responses;
  }

  /**
   * Get an event.
   * @param eventId id of event
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEvent(String eventId, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Get all events based on userId.
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getAllEvents(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Get some events based on userId & event category.
   * @param category event category
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEventsByCategory(String category, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Register user.
   * @param userName user name for AssistMe bot
   * @param userId user id of user that will register to AssistMe bot
   * @param displayName display name of user that will register to AssistMe bot
   * @return list of response message
   */
  public List<Message> register(String userName, String userId, String displayName) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Delete all events.
   * @param userId userId from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteAllEvents(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Delete events by the category.
   * @param category event category
   * @param userId user id from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteEventsByCategory(String category, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Remind a friend who is a user of AssistMe Bot.
   * @param userFrom user who sends the reminder
   * @param userNameTo username who will receive the reminder
   */
  public List<Message> remindFriend(
      LineUser userFrom,
      String userNameTo,
      String reminderMessage
  ) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  /**
   * Get user profile.
   * @param userId user id.
   */
  public List<Message> profile(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_NAME_SUCCESS);
    return responses;
  }

  @Override
  public String toString() {
    return "InputCategoryState";
  }

}