package com.finalproject.assistme.reminder;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReminderTest {

	private Reminder reminder;

	@BeforeEach
	public void setUp() {
		LineUser lineUser = new LineUser("userId","userName","displayName");
  		Event event = new Event(lineUser,"apa aja","apa aja", new Date());
  		Reminder dummy = new Reminder();
  		reminder = new Reminder(event, "janlup");
	}

	@Test
	public void panggilGetEvent() {
		reminder.getEvent();
		assertEquals(true, true);
	}

	@Test
	public void getRemindMessageTest() {
		assertEquals(reminder.getRemindMessage(), "janlup");
	}
}