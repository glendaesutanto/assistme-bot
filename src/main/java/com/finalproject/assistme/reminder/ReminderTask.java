package com.finalproject.assistme.reminder;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.reminder.helper.ScheduledCompletable;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.FlexMessage;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;

public class ReminderTask {

  @Autowired
  private LineUserRepository lineUserRepository;

  private LineMessagingClient lineMessagingClient;
  private EventRepository eventRepository;
  private Reminder reminder;
  private long delayTime;

  /**
   * Constructor for ReminderTask.
   * @param lineMessagingClient line messaging client
   * @param eventRepository event repository
   */
  public ReminderTask(LineMessagingClient lineMessagingClient, EventRepository eventRepository) {
    this.lineMessagingClient = lineMessagingClient;
    this.eventRepository = eventRepository;
  }

  
  /**
   * Setter for reminder and set delay to default (0).
   * @param reminder reminder
   */
  public void setReminder(Reminder reminder) {
    this.reminder = reminder;
    this.delayTime = 0;
  }

  /**
   * Implement reminder with scheduled executor service.
   */
  public void doRemind() {
    Event event = this.reminder.getEvent();
    Date remindTime = event.getReminderTime();
    Instant remindTimeAsInstant = remindTime.toInstant();
    Instant nowTimeAsInstant = new Date().toInstant();
    delayTime = ChronoUnit.MILLIS.between(nowTimeAsInstant, remindTimeAsInstant);
   
    ReminderMessage reminderMessage = new ReminderMessage(this.reminder);
    String userId = event.getUser().getUserId();
    Long eventId = event.getEventId();

    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
    CompletableFuture<FlexMessage> future = ScheduledCompletable.scheduleAsync(
        scheduledExecutorService,
        reminderMessage,
        delayTime,
        TimeUnit.MILLISECONDS
    );
    future.thenAccept(
        answer -> {
          Event eventNow = eventRepository.findEventByEventId(event.getEventId(), userId);
          if (eventNow != null) {
            LineUser userNow = eventRepository.findEventByEventId(event.getEventId(), userId)
                    .getUser();
            PushMessage reminder = new PushMessage(userId, answer);
            lineMessagingClient.pushMessage(reminder);
            event.setIsReminded(true);
            event.setUser(userNow);
            eventRepository.save(event);
          }
          scheduledExecutorService.shutdownNow();
        }
    );
  }
}