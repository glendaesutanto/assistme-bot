package com.finalproject.assistme.state;

import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.message.Message;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class State {
  @Autowired
  LineMessagingClient lineMessagingClient;

  @Autowired
  LineUserRepository lineUserRepository;

  @Autowired
  EventRepository eventRepository;

  protected List<Message> responses;

  public abstract List<Message> createEvent(String userId);

  public abstract List<Message> cancelEvent(String userId);

  public abstract List<Message> deleteEvent(String eventId, String userId);

  public abstract List<Message> others(String[] command, String userId);

  public abstract List<Message> reportMood();

  public abstract List<Message> processMood(String moodType);

  public abstract List<Message> help();

  public abstract List<Message> getEvent(String eventId, String userId);

  public abstract List<Message> getAllEvents(String userId);

  public abstract List<Message> getEventsByCategory(String category,
                                                    String userId);

  public abstract List<Message> register(String userName, String userId, String displayName);

  public abstract List<Message> deleteAllEvents(String userId);

  public abstract List<Message> deleteEventsByCategory(String category, String userId);

  public abstract List<Message> remindFriend(
      LineUser userFrom,
      String userNameTo,
      String reminderMessage
  );

  public abstract List<Message> profile(String userId);

}