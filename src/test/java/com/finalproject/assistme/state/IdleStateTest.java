package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IdleStateTest {

  @Mock
  LineUserRepository lineUserRepository;

  @Mock
  EventRepository eventRepository;

  @Mock
  private CompletableFuture<List<Event>> eventCompletableFuture;

  @Mock
  private CompletableFuture<List<Event>> eventCompletableFuture2;

  @Spy
  @InjectMocks
  private IdleState stateTest;

  @Test
  public void contextLoads() throws Exception {
    assertThat(stateTest).isNotNull();
  }

  @Before
  public void setUp() throws ExecutionException, InterruptedException {
    LineUser userTest = new LineUser("1", "test", "test");
    userTest.setState(IdleState.DB_COL_NAME);
    Event eventTest = new Event(userTest, "test", "test", new Date());
    eventTest.setReminderTime(new Date());
    ArrayList<Event> listEventTest = new ArrayList<>();
    listEventTest.add(eventTest);

    lineUserRepository.save(userTest);
    eventRepository.save(eventTest);

    when(lineUserRepository.findLineUserByUserId("1")).thenReturn(userTest);
    when(lineUserRepository.findLineUserByUserId("2")).thenReturn(null);
    when(eventRepository.findEventByEventId(1l,"1")).thenReturn(eventTest);
    when(eventRepository.findEventByEventId(2l,"1")).thenReturn(null);
    when(eventRepository.findEventsByCategory("general", "1")).thenReturn(listEventTest);
    when(eventRepository.findEventsByCategory("general", "2")).thenReturn(new ArrayList<>());
    when(eventRepository.findEventsByCategory("aku Tidak Ada", "1")).thenReturn(new ArrayList<>());
    when(eventRepository.findEventsByCategory("1", "1")).thenReturn(listEventTest);
    when(eventRepository.findEventsByUserIdAsync("1")).thenReturn(eventCompletableFuture);
    when(eventRepository.findEventsByUserIdAsync("1").get()).thenReturn(listEventTest);
    when(eventRepository.findEventsByUserIdAsync("2")).thenReturn(eventCompletableFuture2);
    when(eventRepository.findEventsByUserIdAsync("2").get()).thenReturn(new ArrayList<>());
    when(eventRepository.findEventsByUserId("1")).thenReturn(listEventTest);
    when(eventRepository.findEventsByUserId("2")).thenReturn(new ArrayList<>());
  }

  @Test
  public void createEventTest() {
    List<Message> response = stateTest.createEvent("1");
    Mockito.verify(stateTest).createEvent("1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void createEventFailTest() {
    List<Message> response = stateTest.createEvent("2");
    Mockito.verify(stateTest).createEvent("2");
    assertTrue(response.size() > 0);
  }

  @Test
  public void cancelEventTest() {
    List<Message> response = stateTest.cancelEvent("1");
    Mockito.verify(stateTest).cancelEvent("1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void deleteEventSuccessTest() {
    List<Message> response = stateTest.deleteEvent("1", "1");
    Mockito.verify(stateTest).deleteEvent("1", "1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void deleteEventFailedTest() {
    List<Message> response = stateTest.deleteEvent("2", "1");
    Mockito.verify(stateTest).deleteEvent("2", "1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void othersTest() {
    List<Message> response = stateTest.others(new String[] {"test"}, "1");
    Mockito.verify(stateTest).others(new String[] {"test"}, "1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void reportMoodTest() {
    List<Message> response = stateTest.reportMood();
    Mockito.verify(stateTest).reportMood();
    assertTrue(response.size() > 0);
  }

  @Test
  public void processMoodTest() {
    List<Message> response = stateTest.processMood("happy");
    Mockito.verify(stateTest).processMood("happy");
    assertTrue(response.size() > 0);
  }

  @Test
  public void helpTest() {
    List<Message> response = stateTest.help();
    Mockito.verify(stateTest).help();
    assertTrue(response.size() > 0);
  }

  @Test
  public void getEventTest() {
    List<Message> response = stateTest.getEvent("1", "1");
    Mockito.verify(stateTest).getEvent("1", "1");
    assertTrue(response.get(0) instanceof FlexMessage);
  }

  @Test
  public void getEventNullTest() {
    List<Message> response = stateTest.getEvent("2", "1");
    Mockito.verify(stateTest).getEvent("2", "1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void getAllEventsTest() throws InterruptedException, ExecutionException {
    List<Message> response = stateTest.getAllEvents("1");
    Mockito.verify(stateTest).getAllEvents("1");
    CompletableFuture<List<Event>> lineUserAsync = eventRepository.findEventsByUserIdAsync("1");
    assertTrue(response.get(0) instanceof FlexMessage);
  }

  @Test
  public void getAllEventsEmptyTest() {
    List<Message> response = stateTest.getAllEvents("2");
    Mockito.verify(stateTest).getAllEvents("2");
    assertTrue(response.get(0).equals(Messages.EVENT_NOT_FOUND));
  }

  @Test
  public void getEventsByCategoryTest() {
    List<Message> response = stateTest.getEventsByCategory("general", "1");
    Mockito.verify(stateTest).getEventsByCategory("general", "1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void getEventsByCategoryEmptyTest() {
    List<Message> response = stateTest.getEventsByCategory("general", "2");
    Mockito.verify(stateTest).getEventsByCategory("general", "2");
    assertTrue(response.size() > 0);
    assertTrue(response.get(0).equals(Messages.noEventWithProvidedCategory("general")));
  }

  @Test
  public void getEventsByCategoryWhenCategoryNotProvidedTest() {
    List<Message> response = stateTest.getEventsByCategory("", "1");
    Mockito.verify(stateTest).getEventsByCategory("", "1");
    assertEquals(Messages.OTHERS_TEXT, response.get(0));
  }

  @Test
  public void registerTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.USER_ALREADY_REGISTERED);
    assertEquals(stateTest.register("test", "1", "test"), response);
  }

  @Test
  public void deleteAllEventsTest() throws InterruptedException, ExecutionException {
    List<Message> response = stateTest.deleteAllEvents("1");
    Mockito.verify(stateTest).deleteAllEvents("1");
    assertEquals(Messages.DELETE_ALL_EVENTS_SUCCESS, response.get(0));
  }

  @Test
  public void deleteAllEventsWhenEventNotExistTest() throws InterruptedException, ExecutionException {
    List<Message> response = stateTest.deleteAllEvents("2");
    Mockito.verify(stateTest).deleteAllEvents("2");
    assertEquals(Messages.EVENT_NOT_FOUND, response.get(0));
  }

  @Test
  public void deleteEventsByCategoryTest() throws InterruptedException, ExecutionException {
    List<Message> response = stateTest.deleteEventsByCategory("1", "1");
    Mockito.verify(stateTest).deleteEventsByCategory("1", "1");
    assertEquals(Messages.deleteEventsByCategorySuccess("1"), response.get(0));
  }

  @Test
  public void deleteEventsByCategoryWhenCategoryNotProvidedTest() throws InterruptedException, ExecutionException {
    List<Message> response = stateTest.deleteEventsByCategory("", "1");
    Mockito.verify(stateTest).deleteEventsByCategory("", "1");
    assertEquals(Messages.OTHERS_TEXT, response.get(0));
  }

  @Test
  public void deleteEventsByCategoryWhenEventNotExistTest() throws InterruptedException, ExecutionException {
    List<Message> response = stateTest.deleteEventsByCategory("aku Tidak Ada", "1");
    Mockito.verify(stateTest).deleteEventsByCategory("aku Tidak Ada", "1");
    assertEquals(Messages.noEventWithProvidedCategory("aku Tidak Ada"), response.get(0));
  }

  @Test
  public void remindFriendWhenFriendNotExistsTest() {
    LineUser userTest = new LineUser("1","1","1");
    List<Message> response = stateTest.remindFriend(userTest, "aloha", "janlup");
    Mockito.verify(stateTest).remindFriend(userTest, "aloha", "janlup");
    assertEquals(Messages.USER_NOT_FOUND, response.get(0));
  }

  @Test
  public void remindFriendWhenFriendExists() {
    LineUser userTest = new LineUser("1","1","1");
    List<Message> response = stateTest.remindFriend(userTest, "test", "janlup");
    Mockito.verify(stateTest).remindFriend(userTest, "test", "janlup");
    assertEquals(response.size() > 0, true);
  }


  @Test
  public void toStringTest() {
    assertEquals("IdleState", stateTest.toString());
  }

  @Test
  public void profileTest() {
    List<Message> response = stateTest.profile("1");
    Mockito.verify(stateTest).profile("1");
    assertTrue(response.get(0) instanceof FlexMessage);
  }

}