package com.finalproject.assistme.reminder;

import com.finalproject.assistme.model.Event;
import org.springframework.stereotype.Component;

@Component
public class Reminder {

  private Event event;
  private String remindMessage;

  public Reminder() {

  }

  public Reminder(Event event, String remindMessage) {
    this.event = event;
    this.remindMessage = remindMessage;
  }

  public Event getEvent() {
    return event;
  }

  public String getRemindMessage() {
    return remindMessage;
  }

}