package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UnregisteredStateTest {

    @Mock
    LineUserRepository lineUserRepository;

    @Spy
    @InjectMocks
    private UnregisteredState stateTest;

    @Test
    public void contextLoads() throws Exception {
        assertThat(stateTest).isNotNull();
    }

    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        when(lineUserRepository.isUserNameNotUnique("notunique")).thenReturn(true);
    }

    @Test
    public void createEventTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.createEvent("1"));
    }

    @Test
    public void cancelEventTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.cancelEvent("1"));
    }

    @Test
    public void deleteEventSuccess() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.deleteEvent("1", "2"));
    }

    @Test
    public void othersTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.others(new String[] {"ha", "hu"},"1"));
    }

    @Test
    public void reportMoodTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.reportMood());
    }

    @Test
    public void processMoodTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.processMood("happy"));
    }

    @Test
    public void helpTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.help());
    }

    @Test
    public void getEventTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.getEvent("1", "1"));
    }

    @Test
    public void getAllEventsTest() throws InterruptedException, ExecutionException {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.getAllEvents("1"));
    }

    @Test
    public void getEventsByCategoryTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.getEventsByCategory("test", "1"));
    }

    @Test
    public void registerTest() throws ExecutionException, InterruptedException {
        List<Message> response = stateTest.register("foobar", "1", "test");
        Mockito.verify(stateTest).register("foobar", "1", "test");
        assertEquals(Messages.registerSuccess("foobar"), response.get(0));
    }

    @Test
    public void registerUserIdNullTest() throws Exception {
        List<Message> response = stateTest.register("foo bar", null, "test");
        Mockito.verify(stateTest).register("foo bar", null, "test");
        assertEquals(Messages.REGISTRATION_ERROR_FETCHING_USERID, response.get(0));
    }

    @Test
    public void registerUserNameFalseWithSpaceTest() throws Exception {
        List<Message> response = stateTest.register("foo bar", "1", "test");
        Mockito.verify(stateTest).register("foo bar", "1", "test");
        assertEquals(Messages.USERNAME_SPEC, response.get(0));
    }

    @Test
    public void registerUserNameFalseWithEnterTest() throws Exception {
        List<Message> response = stateTest.register("foobar\n", "1", "test");
        Mockito.verify(stateTest).register("foobar\n", "1", "test");
        assertEquals(Messages.USERNAME_SPEC, response.get(0));
    }

    @Test
    public void registerUserNameFalseWithLengthMoreThan255Test() throws Exception {
        List<Message> response = stateTest.register("notunique", "1", "test");
        Mockito.verify(stateTest).register("notunique", "1", "test");
        assertEquals(Messages.USERNAME_SPEC, response.get(0));
    }

    @Test
    public void registerUserNameFalseWithUserNameNotUnique() throws Exception {
        List<Message> response = stateTest.register("xYI2l1v6bAW8eCTeVva8nan3jRfEoaGPo77eb6ZbiZ7aRkDJv19QAFSgX28mXcGA1TU0hyUBK24mjy28AdAoW7JVRWqdbPKUD9ACHXk0BRdA63JDCZLFlCjU4Cz3JPgO4kNGbVFejMH9F1qRTLEjGEdmlzhCD2EskVCfnTUayp7toXa69EzwzgyqIaAbpLnOuwoNT6WEAvTD8VghP2vAAsRlbPWfbVjkT1cTOne45iqMgsXEcTGmpn2o8qgM7sPN", "1", "test");
        Mockito.verify(stateTest).register("xYI2l1v6bAW8eCTeVva8nan3jRfEoaGPo77eb6ZbiZ7aRkDJv19QAFSgX28mXcGA1TU0hyUBK24mjy28AdAoW7JVRWqdbPKUD9ACHXk0BRdA63JDCZLFlCjU4Cz3JPgO4kNGbVFejMH9F1qRTLEjGEdmlzhCD2EskVCfnTUayp7toXa69EzwzgyqIaAbpLnOuwoNT6WEAvTD8VghP2vAAsRlbPWfbVjkT1cTOne45iqMgsXEcTGmpn2o8qgM7sPN", "1", "test");
        assertEquals(Messages.USERNAME_SPEC, response.get(0));
    }

    @Test
    public void deleteAllEventsTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.deleteAllEvents("1"));
    }

    @Test
    public void deleteEventsByCategoryTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.deleteEventsByCategory("study","1"));
    }

    @Test
    public void remindFriendTest() {
    LineUser userTest = new LineUser("1","1","1");
    List<Message> response = new ArrayList<>();
    response.add(Messages.USER_NOT_REGISTERED);
    response.add(Messages.USERNAME_SPEC);
    assertEquals(response, stateTest.remindFriend(userTest, "aloha", "janlup"));
  }

    @Test
    public void profileTest() {
        List<Message> response = new ArrayList<>();
        response.add(Messages.USER_NOT_REGISTERED);
        response.add(Messages.USERNAME_SPEC);
        assertEquals(response, stateTest.profile("1"));
    }

    @Test
    public void toStringTest() {
        assertEquals("UnregisteredState", stateTest.toString());
    }
}
