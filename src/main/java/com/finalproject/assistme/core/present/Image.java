package com.finalproject.assistme.core.present;

public class Image {

  private String url;
  
  public Image(String url) {
    this.url = url;
  }

  public String getImage() throws InterruptedException {
    return this.url;
  }
  
}
