package com.finalproject.assistme.state;

import com.finalproject.assistme.core.flexmessage.EventCard;
import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.reminder.service.ReminderService;
import com.linecorp.bot.model.message.Message;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InputEventReminderState extends State {

  @Autowired
  ReminderService reminderService;

  public static final String DB_COL_NAME = "INPUT_REMINDER";

  public InputEventReminderState() {
    this.responses = new ArrayList<>();
  }

  /**
   * Method for creating event at this state.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> createEvent(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Cancel event making.
   * @param userId user ID.
   * @return list of response message.
   */
  public List<Message> cancelEvent(String userId) {
    responses.clear();
    LineUser lineUser = lineUserRepository.findLineUserByUserId(userId);
    Event event = eventRepository.findUnverifiedEventByUserId(userId);

    lineUser.setState(IdleState.DB_COL_NAME);
    lineUserRepository.save(lineUser);
    eventRepository.deleteEventByEventId(event.getEventId(), userId);

    responses.add(Messages.CANCEL_SUCCESS);
    return responses;
  }

  /**
   * Method for deleting event at this state.
   * @param eventId event ID.
   * @param userId user ID.
   * @return list of response messages.
   */
  public List<Message> deleteEvent(String eventId, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Input Event Reminder in this state.
   * @param message Reminder date and message.
   * @param userId user ID.
   * @return list of response message.
   */
  public List<Message> others(String[] message, String userId) {
    responses.clear();
    try {
      Event event = eventRepository.findUnverifiedEventByUserId(userId);
      if (message[0].equals("no")) {
        event.setVefiry(true);
        event.setIsReminded(false);
        event.setReminderTime(event.getDueDateTime());

        LineUser user = event.getUser();
        user.setState(IdleState.DB_COL_NAME);

        eventRepository.save(event);
        lineUserRepository.save(user);

        EventCard eventCard = new EventCard();

        responses.add(Messages.EVENT_SUCCESS);
        responses.add(eventCard.get(event, user.getDisplayName()));

      } else {
        String reminderDateString = this.getMessage(subArray(message, 0, 2));
        String reminderMessage = "Don't forget to do your task!";
        if (message.length > 2) {
          reminderMessage = this.getMessage(subArray(message, 2, message.length));
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date reminderDate = dateFormatter.parse(reminderDateString);
        Date dateNow = new Date();

        if (reminderDate.before(dateNow)
                || reminderDate.after(event.getDueDateTime())) {
          responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
        } else {
          event.setReminderTime(reminderDate);
          event.setVefiry(true);
          LineUser user = event.getUser();
          user.setState(IdleState.DB_COL_NAME);

          eventRepository.save(event);
          lineUserRepository.save(user);
          reminderService.addReminder(event, reminderMessage,
              lineMessagingClient, eventRepository);

          EventCard eventCard = new EventCard();

          responses.add(Messages.EVENT_SUCCESS);
          responses.add(eventCard.get(event, user.getDisplayName()));

        }
      }
    } catch (NullPointerException e) {
      responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    } catch (ParseException e) {
      responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    }
    return responses;

  }


  /**
   * Report mood.
   * @return list of response message
   */
  public List<Message> reportMood() {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Process mood.
   * @param moodType type of the mood
   * @return list of response message
   */
  public List<Message> processMood(String moodType) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Help command is accessed by user.
   * @return list of response message
   */
  public List<Message> help() {
    responses.clear();
    responses.add(Messages.HELP_TEXT);
    return responses;
  }

  /**
   * Get an event.
   * @param eventId id of event
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEvent(String eventId, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Get all events based on userId.
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getAllEvents(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Get some events based on userId & event category.
   * @param category event category
   * @param userId user id of user that own the event
   * @return list of response message
   */
  public List<Message> getEventsByCategory(String category, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Delete all events.
   * @param userId userId from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteAllEvents(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Delete events by the category.
   * @param category event category
   * @param userId user id from a user whose event deleted
   * @return list of response messages.
   */
  public List<Message> deleteEventsByCategory(String category, String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Register user.
   *
   * @param userName user name for AssistMe bot
   * @param userId user id of user that will register to AssistMe bot
   * @param displayName display name of user that will register to AssistMe bot
   * @return list of response message
   */
  public List<Message> register(String userName, String userId, String displayName) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Remind a friend who is a user of AssistMe Bot.
   * @param userFrom user who sends the reminder
   * @param userNameTo username who will receive the reminder
   */
  public List<Message> remindFriend(
      LineUser userFrom, 
      String userNameTo, 
      String reminderMessage
  ) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  /**
   * Get user profile.
   * @param userId user id.
   */
  public List<Message> profile(String userId) {
    responses.clear();
    responses.add(Messages.EVENT_ADD_DUE_DATE_SUCCESS);
    return responses;
  }

  public String getMessage(String[] arrMessage) {
    String joinedMessage = String.join(" ", arrMessage);
    return joinedMessage;
  }

  //references: https://www.techiedelight.com/get-subarray-array-specified-indexes-java/
  public <T> T[] subArray(T[] array, int start, int end) {
    return Arrays.copyOfRange(array, start, end);
  }

  @Override
  public String toString() {
    return "InputEventReminderState";
  }


}
