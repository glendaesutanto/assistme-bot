package com.finalproject.assistme.reminder.service;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.reminder.Reminder;
import com.finalproject.assistme.reminder.ReminderTask;
import com.finalproject.assistme.repository.EventRepository;
import com.linecorp.bot.client.LineMessagingClient;
import org.springframework.stereotype.Service;

@Service
public class ReminderServiceImpl implements ReminderService {

  @Override
  public void addReminder(
      Event event,
      String remindMessage,
      LineMessagingClient lineMessagingClient,
      EventRepository eventRepository
  ) {

    Reminder reminder = new Reminder(event, remindMessage);
    ReminderTask reminderTask = new ReminderTask(lineMessagingClient, eventRepository);
    reminderTask.setReminder(reminder);
    reminderTask.doRemind();
  }
}