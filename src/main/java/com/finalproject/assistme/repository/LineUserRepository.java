package com.finalproject.assistme.repository;

import com.finalproject.assistme.model.LineUser;
import java.util.concurrent.CompletableFuture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

@Repository
public interface LineUserRepository extends JpaRepository<LineUser, String> {
  @Query(value = "SELECT * FROM LINE_USER WHERE USER_NAME = ?1", nativeQuery = true)
  LineUser findLineUserByUserName(String userName);


  @Query(value = "SELECT * FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
  LineUser findLineUserByUserId(String userID);

  @Async
  @Query(value = "SELECT * FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
  CompletableFuture<LineUser> findLineUserByUserIdAsync(String userID);

  @Query(value =
      "SELECT CASE WHEN COUNT(*) > 0 THEN true ELSE false END FROM LINE_USER as U "
      + "WHERE U.USER_ID = ?1",
      nativeQuery = true)
  boolean isLineUserRegistered(String userId);

  @Query(value =
      "SELECT CASE WHEN COUNT(*) > 0 THEN true ELSE false END FROM LINE_USER as U "
      + "WHERE U.USER_NAME = ?1",
      nativeQuery = true
  )
  boolean isUserNameNotUnique(String userName);

  @Query(value = "SELECT state FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
  String findStateByUserId(String userID);

}

