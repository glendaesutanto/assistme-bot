package com.finalproject.assistme.reminder;

import com.finalproject.assistme.core.flexmessage.ReminderCard;
import com.finalproject.assistme.model.Event;
import com.linecorp.bot.model.message.FlexMessage;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class ReminderMessage implements Supplier<CompletableFuture<FlexMessage>> {

  private Reminder reminder;

  public ReminderMessage(Reminder reminder) {
    this.reminder = reminder;
  }

  @Override
  public CompletableFuture<FlexMessage> get() {
    Event event = reminder.getEvent();
    String remindMessage = reminder.getRemindMessage();

    FlexMessage reminderCard = new ReminderCard().get(event, remindMessage);

    return CompletableFuture.completedFuture(reminderCard);
  }
}