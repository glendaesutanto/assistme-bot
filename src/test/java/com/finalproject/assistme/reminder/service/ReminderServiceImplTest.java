package com.finalproject.assistme.reminder.service;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Date;

public class ReminderServiceImplTest {

	// @Mock
	private ReminderServiceImpl reminderService;

	@Mock
	private LineMessagingClient lineMessagingClient;

	@Mock
	private EventRepository eventRepository;

	@Mock
	private LineUserRepository lineUserRepository;

	private Event event;

	@BeforeEach
	public void setUp() {
		reminderService = new ReminderServiceImpl();
		LineUser lineUser = new LineUser("userId","userName","displayName");
  		event = new Event(lineUser,"apa aja","apa aja", new Date());
  		event.setReminderTime(new Date());
	}

	@Test
	public void callAddReminderTest() {
		reminderService.addReminder(event, "janlup", lineMessagingClient, eventRepository);
	}
}