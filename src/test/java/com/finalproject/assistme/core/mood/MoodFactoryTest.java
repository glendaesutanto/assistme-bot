package com.finalproject.assistme.core.mood;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertTrue;

public class MoodFactoryTest {

  private MoodFactory moodFactory;

  @BeforeEach
  public void setUp() {
    moodFactory = new MoodFactory();
  }

  @Test
  public void testMethodCreateHappyMood() {
    Mood happyMood = moodFactory.createMood("happy");
    assertTrue(happyMood instanceof HappyMood);
  }

  @Test
  public void testMethodCreateSadMood() {
    Mood sadMood = moodFactory.createMood("sad");
    assertTrue(sadMood instanceof SadMood);
  }

  @Test
  public void testMethodCreateOtherMood() {
    Mood otherMood = moodFactory.createMood("test");
    assertTrue(otherMood instanceof OtherMood);
  }

  @Test
  public void testMethodCreateProductiveMood() {
    Mood productiveMood = moodFactory.createMood("productive");
    assertTrue(productiveMood instanceof ProductiveMood);
  }

  @Test
  public void testMethodCreateTiredMood() {
    Mood tiredMood = moodFactory.createMood("tired");
    assertTrue(tiredMood instanceof TiredMood);
  }
}