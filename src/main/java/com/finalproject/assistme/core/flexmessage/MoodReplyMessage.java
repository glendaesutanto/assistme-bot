package com.finalproject.assistme.core.flexmessage;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;
import java.util.ArrayList;
import java.util.List;

public class MoodReplyMessage {

  private final QuickReplyItem happyQuickReply = QuickReplyItem.builder()
      .action(new MessageAction("Happy","/mood happy"))
      .build();
  private final QuickReplyItem sadQuickReply = QuickReplyItem.builder()
      .action(new MessageAction("Sad","/mood sad"))
      .build();
  private final QuickReplyItem tiredQuickReply = QuickReplyItem.builder()
      .action(new MessageAction("Tired","/mood tired"))
      .build();
  private final QuickReplyItem productiveQuickReply = QuickReplyItem.builder()
      .action(new MessageAction("Productive","/mood productive"))
      .build();
  private final List<QuickReplyItem> moodQuickReplyItems = new ArrayList<>();
  
  /**
   * Constructor for MoodReplyMessage.
   * Add mood quick reply items to list
   */
  public MoodReplyMessage() {
    moodQuickReplyItems.add(happyQuickReply);
    moodQuickReplyItems.add(sadQuickReply);
    moodQuickReplyItems.add(tiredQuickReply);
    moodQuickReplyItems.add(productiveQuickReply);
  }

  /**
   * Show quickreply message to choose the mood.
   * @return quickreply message and text message for description
   */
  public TextMessage getMoodReplyMessage() {
    final QuickReply moodQuickReply = QuickReply.items(moodQuickReplyItems);
    
    return TextMessage
        .builder()
        .text("What's your mood right now? Please select or reply with '/mood [yourMood]'")
        .quickReply(moodQuickReply)
        .build();
  }
}