package com.finalproject.assistme.core.present;

import com.finalproject.assistme.musicplayer.Spotify;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SpotifyPlaylistTest {

  private SpotifyPlaylist spotifyPlaylist;

  @BeforeEach
  public void setUp() {
    Spotify.clientCredentialsSync();
    spotifyPlaylist = new SpotifyPlaylist();
  }


  @Test
  public void getTracksTest() {
    assertEquals(null, spotifyPlaylist.getTracks("3dV99CACGvTGr8qkm9leud"));
  }
}