package com.finalproject.assistme.handler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationServiceTest {

	private ApplicationService applicationService;
	private Handler handler;

	@BeforeEach
	public void setUp() {
		handler = new UniversalHandler();
		ApplicationService dummy = new ApplicationService();
		applicationService = new ApplicationService(handler);
	}

	@Test
	public void call() {
		applicationService.getResponse();
		applicationService.setHandler(new UniversalHandler());
		assertEquals(true, true);
	}
}