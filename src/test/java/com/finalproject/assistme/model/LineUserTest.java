package com.finalproject.assistme.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LineUserTest {

  private LineUser lineUser;

  @BeforeEach
  public void setUp() {
    this.lineUser = new LineUser("1", "nori", "noriuhuy");
  }

  @Test
  public void testSetGetUserId() {
    assertEquals("1", this.lineUser.getUserId());
    this.lineUser.setUserId("10l");
    assertTrue(this.lineUser.getUserId() == "10l");
  }

  @Test
  public void testSetGetUserName() {
    assertEquals("nori", this.lineUser.getUserName());
    this.lineUser.setUserName("nora");
    assertEquals("nora", this.lineUser.getUserName());
  }

  @Test
  public void testSetGetDisplayName() {
    assertEquals("noriuhuy", this.lineUser.getDisplayName());
    this.lineUser.setDisplayName("norauhuy");
    assertEquals("norauhuy", this.lineUser.getDisplayName());
  }


  @Test
  public void testSetGetState() {
    assertEquals("UNREGISTERED", this.lineUser.getState());
    this.lineUser.setState("unregistered");
    assertEquals("unregistered", this.lineUser.getState());
  }

  @Test
  public void testUserNameUnique() {
    this.lineUser = new LineUser();
    this.lineUser.setUserId("1");
    this.lineUser.setUserName("nora");
    this.lineUser.setDisplayName("noriuhuy");
  }

}
