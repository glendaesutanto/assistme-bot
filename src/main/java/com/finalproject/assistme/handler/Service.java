package com.finalproject.assistme.handler;

import com.linecorp.bot.model.message.Message;
import java.util.List;

public interface Service {

  List<Message> getResponse();

  void setHandler(Handler handler);
}