package com.finalproject.assistme.core.mood;

import com.finalproject.assistme.core.present.Image;
import com.finalproject.assistme.core.present.ImageUrlList;
import com.finalproject.assistme.core.present.Quote;
import com.finalproject.assistme.core.present.QuoteList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProductiveMoodTest {

  private Mood mood;
  private ImageUrlList imageUrlList;
  private QuoteList quoteList;

  @BeforeEach
  public void setUp() {
    mood = new ProductiveMood();
    imageUrlList = new ImageUrlList();
    quoteList = new QuoteList();
  }

  @Test
  public void testMethodGetType() {
    assertEquals("Productive",mood.getType());
  }

  @Test
  public void testMethodGetImage() {
    Image image = mood.getImage();
    try {
      assertTrue(Arrays.asList(imageUrlList.productiveList).contains(image.getImage()));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testMethodGetQuote() {
    Quote quote = mood.getQuote();
    try {
      assertTrue(Arrays.asList(quoteList.productiveList).contains(quote.getQuote()));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }


}
