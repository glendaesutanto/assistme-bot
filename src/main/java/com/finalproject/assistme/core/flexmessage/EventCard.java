package com.finalproject.assistme.core.flexmessage;

import static java.util.Arrays.asList;

import com.finalproject.assistme.model.Event;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventCard {

  /**
   * Get message to show event detail.
   * @param event Event.
   * @return FlexMessage.
   */
  public FlexMessage get(Event event, String user) {

    final Box bodyBlock = createBodyBlock(
            event.getEventId(),
            event.getCategory(),
            event.getCreatedAt(),
            event.getName(),
            event.getDueDateTime(),
            event.getReminderTime(),
            user);
    final Box footerBlock = createFooterBlock(event.getEventId());

    final Bubble bubble =
        Bubble.builder()
            .body(bodyBlock)
            .footer(footerBlock)
            .build();

    return new FlexMessage("AssistMe-Bot sent you an event card",bubble);
  }

  private Box createBodyBlock(Long eventId, String category, Date createdAt, String name,
        Date dueDate, Date reminderTime, String user) {

    String createdAtAsString = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(createdAt);
    String dueDateAsString = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(dueDate);
    String reminderTimeAsString = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(reminderTime);

    final Text header = Text.builder()
        .text("EVENT #" + eventId + " DETAIL")
        .weight(TextWeight.BOLD)
        .color("#c18cdb")
        .size(FlexFontSize.SM)
        .build();

    final Text title = Text.builder()
        .text(name)
        .weight(TextWeight.BOLD)
        .size(FlexFontSize.Md)
        .margin(FlexMarginSize.MD)
        .wrap(true)
        .build();

    final Separator separator = Separator.builder().margin(FlexMarginSize.MD).build();

    final Box categoryBox = createHorizontalInfoBlock("Category", category);
    final Box createdAtBox = createHorizontalInfoBlock("Created at", createdAtAsString);
    final Box dueDateBox = createHorizontalInfoBlock("Due date", dueDateAsString);
    final Box reminderTimeBox = createHorizontalInfoBlock("Reminder time", reminderTimeAsString);

    final Box infoBox = Box.builder()
        .layout(FlexLayout.VERTICAL)
        .margin(FlexMarginSize.MD)
        .spacing(FlexMarginSize.SM)
        .contents(asList(categoryBox, createdAtBox, dueDateBox, reminderTimeBox))
        .build();

    final Box createdByBox = createHorizontalCreatedByBlock("Created by", user);

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(header, title, separator, infoBox, separator, createdByBox))
        .build();

  }

  private Box createHorizontalInfoBlock(String label, String content) {

    final Text labelText = Text.builder()
        .text(label)
        .size(FlexFontSize.SM)
        .color("#555555")
        .flex(0)
        .wrap(true)
        .align(FlexAlign.START)
        .build();

    final Text contentText = Text.builder()
        .text(content)
        .size(FlexFontSize.SM)
        .color("#111111")
        .wrap(true)
        .align(FlexAlign.END)
        .build();

    return Box.builder()
        .layout(FlexLayout.HORIZONTAL)
        .contents(asList(labelText, contentText))
        .build();
  }

  private Box createHorizontalCreatedByBlock(String label, String content) {

    final Text labelText = Text.builder()
        .text(label)
        .size(FlexFontSize.XS)
        .color("#aaaaaa")
        .flex(0)
        .wrap(true)
        .align(FlexAlign.START)
        .build();

    final Text contentText = Text.builder()
        .text(content)
        .size(FlexFontSize.XS)
        .color("#aaaaaa")
        .wrap(true)
        .align(FlexAlign.END)
        .build();

    return Box.builder()
        .layout(FlexLayout.HORIZONTAL)
        .margin(FlexMarginSize.MD)
        .contents(asList(labelText, contentText))
        .build();
  }

  private Box createFooterBlock(Long eventId) {
    final Button deleteAction = Button
        .builder()
        .style(ButtonStyle.PRIMARY)
        .height(ButtonHeight.SMALL)
        .margin(FlexMarginSize.XS)
        .color("#c18cdb")
        .action(new MessageAction("Delete Event", "/deleteEvent " + eventId))
        .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .spacing(FlexMarginSize.SM)
        .contents(asList(deleteAction))
        .build();
  }
}