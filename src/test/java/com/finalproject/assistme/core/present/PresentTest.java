package com.finalproject.assistme.core.present;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PresentTest {

  private Present present;
  private Image image;
  private Quote quote;
  private SpotifyPlaylist spotifyPlaylist;

  @BeforeEach
  public void setUp() {
    image = new Image("dummyImage");
    quote = new Quote("dummyQuote");
    spotifyPlaylist = new SpotifyPlaylist();
    present = new Present(image, quote, spotifyPlaylist);
  }

  @Test
  public void testGetImageMethod() {
    assertEquals(image, present.getImage());
  }

  @Test
  public void testGetQuoteMethod() {
    assertEquals(quote, present.getQuote());
  }

  @Test
  public void testGetSpotifyPlaylistMethod() {
    assertEquals(spotifyPlaylist, present.getSpotifyPlaylist());
  }
}