package com.finalproject.assistme.core.flexmessage;

import com.finalproject.assistme.model.LineUser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FriendReminderCardTest {

	@Test
	public void testGetMessageMethod() {
		LineUser lineUserFrom = new LineUser("userId","userName","displayName");
		LineUser lineUserTo = new LineUser("userId2","userName2","displayName2");
    	FriendReminderCard friendReminderCard = new FriendReminderCard();
		assertEquals("Your friend sent you a reminder!",
      		friendReminderCard.get(lineUserFrom, lineUserTo, "janlup").getAltText());
	}
}