package com.finalproject.assistme.core.present;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class QuoteTest {

  private Quote quote;

  @BeforeEach
  public void setUp() {
    quote = new Quote("dummy");
  }

  @Test
  public void testGetQuoteMethod() {
    try {
      assertEquals("dummy", quote.getQuote());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}