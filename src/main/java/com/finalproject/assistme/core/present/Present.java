package com.finalproject.assistme.core.present;

public class Present {

  private Image image;
  private Quote quote;
  private SpotifyPlaylist spotifyPlaylist;

  /**
   * Present constructor.
   * @param image represents the image
   * @param quote represents the quote
   * @param spotifyPlaylist represents the spotify playlist
   */
  public Present(Image image, Quote quote, SpotifyPlaylist spotifyPlaylist) {
    this.image = image;
    this.quote = quote;
    this.spotifyPlaylist = spotifyPlaylist;
  }

  public Image getImage() {
    return this.image;
  }

  public Quote getQuote() {
    return this.quote;
  }

  public SpotifyPlaylist getSpotifyPlaylist() {
    return this.spotifyPlaylist;
  }
  
}
