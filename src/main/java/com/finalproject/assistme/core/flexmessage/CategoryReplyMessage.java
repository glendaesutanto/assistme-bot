package com.finalproject.assistme.core.flexmessage;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;
import java.util.ArrayList;
import java.util.List;

public class CategoryReplyMessage {
  private final QuickReplyItem workQuickReply = QuickReplyItem.builder()
      .action(new MessageAction("Work", "work"))
      .build();
  private final QuickReplyItem healthQuickReply = QuickReplyItem.builder()
      .action(new MessageAction("Health", "health"))
      .build();
  private final QuickReplyItem lifeQuickReply = QuickReplyItem.builder()
      .action(new MessageAction("Life", "life"))
      .build();
  private final List<QuickReplyItem> categoryQuickReplyItems = new ArrayList<>();

  /**
   * Get CategoryReplyMessage object.
   */
  public CategoryReplyMessage() {
    categoryQuickReplyItems.add(workQuickReply);
    categoryQuickReplyItems.add(healthQuickReply);
    categoryQuickReplyItems.add(lifeQuickReply);
  }

  /**
   * Get quickreply message for categories.
   * @return TextMessage
   */
  public TextMessage getCategoryReplyMessage() {
    final QuickReply categorQuickReply = QuickReply.items(categoryQuickReplyItems);

    return TextMessage
      .builder()
      .text("Please choose category for the event\n"
              + "or type the name of the category\nor type /cancel")
      .quickReply(categorQuickReply)
      .build();
  }


}
