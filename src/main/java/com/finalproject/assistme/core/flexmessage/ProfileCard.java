package com.finalproject.assistme.core.flexmessage;

import static java.util.Arrays.asList;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;


public class ProfileCard {

  /**
   * Get message to show profile from user.
   * @param displayName display name from lineUser.
   * @param userName user name from lineUser.
   * @return FlexMessage.
   */
  public FlexMessage get(String displayName, String userName) {

    final Box bodyBlock = createBodyBlock(displayName, userName);

    final Bubble bubble =
        Bubble.builder()
            .body(bodyBlock)
            .build();

    return new FlexMessage("AssistMe-Bot sent you an profile card", bubble);
  }

  private Box createBodyBlock(String displayName, String userName) {

    final Text header = Text.builder()
        .text("PROFILE")
        .weight(TextWeight.BOLD)
        .color("#c18cdb")
        .size(FlexFontSize.SM)
        .build();

    final Separator separator = Separator.builder().margin(FlexMarginSize.MD).build();

    final Text title = Text.builder()
        .text(displayName)
        .weight(TextWeight.BOLD)
        .margin(FlexMarginSize.MD)
        .align(FlexAlign.CENTER)
        .wrap(true)
        .build();

    final Text userNameText = Text.builder()
            .text("Username:")
            .size(FlexFontSize.SM)
            .margin(FlexMarginSize.MD)
            .color("#aaaaaa")
            .build();

    final Text userNameLineUser = Text.builder()
            .text(userName)
            .size(FlexFontSize.XS)
            .margin(FlexMarginSize.XS)
            .color("#aaaaaa")
            .wrap(true)
            .build();

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .contents(asList(header, separator, title, separator, userNameText, userNameLineUser))
        .build();

  }

}