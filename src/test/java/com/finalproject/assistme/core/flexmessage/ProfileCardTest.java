package com.finalproject.assistme.core.flexmessage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProfileCardTest {

  @Test
  public void testGetMessageMethod() {
    ProfileCard profileCard = new ProfileCard();
    assertEquals("AssistMe-Bot sent you an profile card", profileCard.get("testDisplay", "test").getAltText());
  }
}
