package com.finalproject.assistme.reminder.service;

import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.repository.EventRepository;
import com.linecorp.bot.client.LineMessagingClient;

public interface ReminderService {

  public void addReminder(
      Event event,
      String remindMessage,
      LineMessagingClient lineMessagingClient,
      EventRepository eventRepository
  );

}