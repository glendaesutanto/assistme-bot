package com.finalproject.assistme.state;

import com.finalproject.assistme.handler.message.Messages;
import com.finalproject.assistme.model.Event;
import com.finalproject.assistme.model.LineUser;
import com.finalproject.assistme.repository.EventRepository;
import com.finalproject.assistme.repository.LineUserRepository;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InputCategoryStateTest {

  @Mock
  LineUserRepository lineUserRepository;

  @Mock
  EventRepository eventRepository;

  @Spy
  @InjectMocks
  private InputCategoryState stateTest;

  @Test
  public void contextLoads() throws Exception {
    assertThat(stateTest).isNotNull();
  }

  @Before
  public void setUp() {
    LineUser userTest = new LineUser("1", "test", "test");
    userTest.setState(InputCategoryState.DB_COL_NAME);
    Event eventTest = new Event(userTest, "test", "test", new Date());

    lineUserRepository.save(userTest);
    eventRepository.save(eventTest);

    when(eventRepository.findUnverifiedEventByUserId("1")).thenReturn(eventTest);
    when(eventRepository.findUnverifiedEventByUserId("2")).thenReturn(null);
    when(lineUserRepository.findLineUserByUserId("1")).thenReturn(userTest);
  }

  @Test
  public void createEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.createEvent("1"), response);
  }

  @Test
  public void cancelEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.CANCEL_SUCCESS);
    assertEquals(stateTest.cancelEvent("1"), response);
  }

  @Test
  public void deleteEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.deleteEvent("1", "1"), response);
  }

  @Test
  public void othersTest() {
    List<Message> response = stateTest.others(new String[] {"work"}, "1");
    assertTrue(response.size() > 0);
  }

  @Test
  public void othersTestNull() {
    List<Message> response = stateTest.others(new String[] {"work"}, "2");
    assertTrue(response.size() > 0);
  }

  @Test
  public void reportMoodTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.reportMood(), response);
  }

  @Test
  public void processMoodTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.processMood("happy"), response);
  }

  @Test
  public void helpTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.HELP_TEXT);
    assertEquals(stateTest.help(), response);
  }

  @Test
  public void getEventTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.getEvent("1", "1"), response);
  }

  @Test
  public void getAllEventsTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.getAllEvents("1"), response);
  }

  @Test
  public void getEventsByCategoryTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.getEventsByCategory("test", "1"), response);
  }

  @Test
  public void registerTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(stateTest.register("test", "1", "test"), response);
  }

  @Test
  public void deleteAllEventsTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(response, stateTest.deleteAllEvents("1"));
  }

  @Test
  public void deleteEventsByCategoryTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(response, stateTest.deleteEventsByCategory("study","1"));
  }

  @Test
  public void remindFriendTest() {
    LineUser userTest = new LineUser("1","1","1");
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(response, stateTest.remindFriend(userTest, "aloha", "janlup"));
  }

  @Test
  public void profileTest() {
    List<Message> response = new ArrayList<>();
    response.add(Messages.EVENT_ADD_NAME_SUCCESS);
    assertEquals(response, stateTest.profile("1"));
  }

  @Test
  public void toStringTest() {
    assertEquals("InputCategoryState", stateTest.toString());
  }

}