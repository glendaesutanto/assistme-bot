package com.finalproject.assistme.state.helper;

import com.finalproject.assistme.repository.LineUserRepository;
import com.finalproject.assistme.state.IdleState;
import com.finalproject.assistme.state.InputCategoryState;
import com.finalproject.assistme.state.InputEventNameState;
import com.finalproject.assistme.state.InputEventReminderState;
import com.finalproject.assistme.state.InputEventTimeState;
import com.finalproject.assistme.state.State;
import com.finalproject.assistme.state.UnregisteredState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateHelper {
    
  @Autowired
  LineUserRepository lineUserRepository;

  @Autowired
  private IdleState idleState;

  @Autowired
  private InputCategoryState inputCategoryState;

  @Autowired
  private InputEventNameState inputEventNameState;

  @Autowired
  private InputEventTimeState inputEventTimeState;

  @Autowired
  private InputEventReminderState inputEventReminderState;

  @Autowired
  private UnregisteredState unregisteredState;

  /**
   * Change state in the fly.
   * @param stateString state.DB_COL_NAME.
   * @return state to go.
   */
  public State toState(String stateString) {
    switch (stateString) {
      case InputCategoryState.DB_COL_NAME:
        return inputCategoryState;
      case InputEventNameState.DB_COL_NAME:
        return inputEventNameState;
      case InputEventTimeState.DB_COL_NAME:
        return inputEventTimeState;
      case InputEventReminderState.DB_COL_NAME:
        return inputEventReminderState;
      case IdleState.DB_COL_NAME:
        return idleState;
      default:
        return unregisteredState;
    }
  }

  /**
   * Get user state by Id.
   * @param userId user ID.
   * @return state of the user.
   */
  public State getUserState(String userId) {
    State state;
    if (lineUserRepository.isLineUserRegistered(userId)) {
      String stateString = lineUserRepository.findStateByUserId(userId);
      state = toState(stateString);
    } else {
      state = unregisteredState;
    }
    return state;
  }
}