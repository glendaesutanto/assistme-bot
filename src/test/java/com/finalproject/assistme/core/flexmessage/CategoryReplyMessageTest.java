package com.finalproject.assistme.core.flexmessage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CategoryReplyMessageTest {
    
  @Test
  public void getCategoryReplyMessageTest() {
    CategoryReplyMessage message = new CategoryReplyMessage();
    assertEquals("Please choose category for the event\nor type the name of the category\nor type /cancel",
            message.getCategoryReplyMessage().getText());
  }
}